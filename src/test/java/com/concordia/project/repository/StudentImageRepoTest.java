package com.concordia.project.repository;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.concordia.project.model.StudentImage;

/**
 * CEJV 559 - Group 2 Project
 * StudentImage repository units tests
 * Implementation with CRUD and pagination
 *
 * @author Tetiana, Marchiano
 *
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class StudentImageRepoTest {
	
	@Autowired
	private StudentImageRepository studentImageRepository;

	@Test
	public void testSaveStudentImage() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		StudentImage studentImage2 = studentImageRepository.save(studentImage);
		assertThat(studentImage2.getStudentImageId()).isGreaterThan(0);
	}

	@Test
	public void testGetStudentImage() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		studentImageRepository.save(studentImage);
		StudentImage studentImage1 = studentImageRepository.findByStudentImageCode("img001");
		assertNotNull(studentImage);
		assertEquals(studentImage.getStudentImageCode(), studentImage1.getStudentImageCode());
	}

	@Test
	public void testDeleteStudentImage() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		studentImageRepository.save(studentImage);
		studentImageRepository.delete(studentImage);
	}

	@Test
	public void findAllStudentImages() {
		int totalRecords = 5;
		for (int record = 0; record < totalRecords; record++) {
			StudentImage studentImage = new StudentImage("img" + record, "test_image.png");
			studentImageRepository.save(studentImage);
		}
		assertEquals(studentImageRepository.count(), totalRecords);
		assertNotNull(studentImageRepository.findAll());
	}

	@Test
	public void deleteByStudentImageIdTest() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		StudentImage studentImage1 = studentImageRepository.save(studentImage);
		studentImageRepository.deleteById(studentImage1.getStudentImageId());
	}

	@Test
	public void testUpdateStudentImage() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		studentImageRepository.save(studentImage);
		assertNotNull(studentImage);
		studentImage.setStudentImageCode("img002");
		assertEquals("img002", studentImage.getStudentImageCode());
	}

	@Test
	public void testPaginationStudentImages() {
		int totalStudentImages = 100;
		for (int studentImageNumber = 0; studentImageNumber < totalStudentImages; studentImageNumber++) {
			StudentImage studentImage = new StudentImage("img" + studentImageNumber, "test_image.png");
			studentImageRepository.save(studentImage);
		}

		assertEquals(totalStudentImages, studentImageRepository.count());

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());
		Page<StudentImage> studentImagePageResult = studentImageRepository.findAll(paging);

		if (studentImagePageResult.hasContent()) {
			assertEquals(studentImagePageResult.getContent().size(), 10);
		}
	}

	@Test
	public void findByStudentImageCodeTest() {
		StudentImage studentImage = new StudentImage("img001", "test_image.png");
		studentImageRepository.save(studentImage);
		StudentImage studentImage1 = studentImageRepository.findByStudentImageCode(studentImage.getStudentImageCode());
		assertEquals(studentImage.getStudentImageCode(), studentImage1.getStudentImageCode());
	}


}
