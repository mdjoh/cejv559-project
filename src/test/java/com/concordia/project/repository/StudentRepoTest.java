package com.concordia.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.concordia.project.model.Registration;
import com.concordia.project.model.Student;


/**
 * CEJV 559 - Group 2 Project
 * Student repository units tests
 * Implementation with CRUD and pagination
 *
 * @author Tetiana
 *
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class StudentRepoTest {
	
	@Autowired
	private StudentRepository studentRepository;

	@Test
	public void testSaveStudent() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		student.setRegistration(null);
		studentRepository.save(student);
		Student student1 = studentRepository.findStudentByFirstNameAndLastName("John", "Doe");
		assertNotNull(student);
		assertEquals(student.getFirstName(), student1.getFirstName());
		assertEquals(student.getLastName(), student1.getLastName());
		assertEquals(student.getStudentEmail(), student1.getStudentEmail());
	}

	@Test
	public void testGetStudent() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		studentRepository.save(student);
		Student student1 = studentRepository.findStudentByFirstNameAndLastName("John", "Doe");
		assertNotNull(student);
		assertEquals(student.getFirstName(), student1.getFirstName());
		assertEquals(student.getLastName(), student1.getLastName());
		assertEquals(student.getStudentEmail(), student1.getStudentEmail());
	}

	@Test
	public void testDeleteStudent() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		studentRepository.save(student);
		studentRepository.delete(student);
	}

	@Test
	public void findAllStudents() {
		int totalRecords = 5;
		for (int record = 0; record < totalRecords; record++) {
			Student student = new Student("John" + record, "Doe" + record, "johndoe@mail.com" + record);
			studentRepository.save(student);
		}
		assertEquals(studentRepository.count(), totalRecords);
		assertNotNull(studentRepository.findAll());
	}

	@Test
	public void deleteByStudentIdTest() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		Student student1 = studentRepository.save(student);
		studentRepository.deleteById(student1.getStudentId());
	}

	@Test
	public void testUpdateStudent() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		studentRepository.save(student);
		assertNotNull(student);
		student.setFirstName("Jane");
		student.setLastName("Smith");
		student.setStudentEmail("janesmith@mail.com");
		assertEquals("Jane", student.getFirstName());
		assertEquals("Smith", student.getLastName());
		assertEquals("janesmith@mail.com", student.getStudentEmail());
	}

	@Test
	public void testPaginationStudents() {
		int totalStudents = 100;
		for (int studentNumber = 0; studentNumber < totalStudents; studentNumber++) {
			Student student = new Student("John" + studentNumber, "Doe" + studentNumber,
					"johndoe@mail.com" + studentNumber);
			studentRepository.save(student);
		}

		assertEquals(totalStudents, studentRepository.count());

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());
		Page<Student> studentPageResult = studentRepository.findAll(paging);

		if (studentPageResult.hasContent()) {
			assertEquals(studentPageResult.getContent().size(), 10);
		}
	}

	@Test
	public void findByStudentCodeTest() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		studentRepository.save(student);
		Student student1 = studentRepository.findByStudentCode(student.getStudentCode());
		assertEquals(student.getStudentCode(), student1.getStudentCode());
	}

	@Test
	public void findByStudentEmailTest() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");
		studentRepository.save(student);
		Student student1 = studentRepository.findByStudentEmail(student.getStudentEmail());
		assertEquals(student.getStudentEmail(), student1.getStudentEmail());
	}

	// JUnit to add when Registration model is added
	@Test
	public void testSaveRegistration() {
		Student student = new Student("John", "Doe", "johndoe@mail.com");

		Set<Registration> registrations = new HashSet<Registration>();

		Registration registration1 = new Registration("abc");
		registration1.setStudent(student);
		registrations.add(registration1);

		Registration registration2 = new Registration("def");
		registration2.setStudent(student);
		registrations.add(registration2);

		Registration registration3 = new Registration("ghi");
		registration3.setStudent(student);
		registrations.add(registration3);

		student.setRegistration(registrations);
		studentRepository.save(student);
		Student student1 = studentRepository.findStudentByFirstNameAndLastName("John", "Doe");
		assertNotNull(student);
		assertEquals(student.getFirstName(), student1.getFirstName());
		assertEquals(student.getLastName(), student1.getLastName());
		assertEquals(student.getStudentEmail(), student1.getStudentEmail());
		assertEquals(student.getDisabled(), student1.getDisabled());
		assertEquals(student1.getRegistration().size(), 3);

	}
}
