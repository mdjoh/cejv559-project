package com.concordia.project.repository;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.concordia.project.model.Registration;
import com.concordia.project.model.Term;

/**
 * CEJV 559 - Group 2 Project Term repository units tests Implementation with
 * CRUD and pagination
 *
 * @author Tetiana
 *
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.ANY)
public class TermRepoTest {

	@Autowired
	private TermRepository termRepository;

	@Test
	public void testSaveTerm() {
		Term term = new Term("Spring 2021");
		term.setRegistration(null);
		termRepository.save(term);
		Term term1 = termRepository.findByDescription("Spring 2021");
		assertNotNull(term);
		assertEquals(term.getDescription(), term1.getDescription());
	}

	@Test
	public void testGetTerm() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		Term term1 = termRepository.findByDescription("Spring 2021");
		assertNotNull(term);
		assertEquals(term.getDescription(), term1.getDescription());
		assertEquals(term.getRegistration(), term1.getRegistration());
		assertEquals(term.getTermCode(), term1.getTermCode());
	}

	@Test
	public void testDeleteTerm() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		termRepository.delete(term);
	}

	@Test
	public void findAllTerms() {
		int totalRecords = 5;
		for (int record = 0; record < totalRecords; record++) {
			Term term = new Term("Spring 2021" + "-" + record);
			termRepository.save(term);
		}
		assertEquals(termRepository.count(), totalRecords);
		assertNotNull(termRepository.findAll());
	}

	@Test
	public void deleteByTermIdTest() {
		Term term = new Term("Spring 2021");
		Term term1 = termRepository.save(term);
		termRepository.deleteById(term1.getTermId());
	}

	@Test
	public void testUpdateTerm() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		assertNotNull(term);
		term.setDescription("Winter2020");
		assertEquals("Winter2020", term.getDescription());
	}

	@Test
	public void testPaginationTerms() {
		int totalTerms = 100;
		for (int termNumber = 0; termNumber < totalTerms; termNumber++) {
			Term term = new Term("Spring 2021" + "-" + termNumber);
			termRepository.save(term);
		}

		assertEquals(totalTerms, termRepository.count());

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());
		Page<Term> termPageResult = termRepository.findAll(paging);

		if (termPageResult.hasContent()) {
			assertEquals(termPageResult.getContent().size(), 10);
		}
	}

	@Test
	public void findByTermCodeTest() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		Term term1 = termRepository.findByTermCode(term.getTermCode());
		assertEquals(term.getTermCode(), term1.getTermCode());
	}

	@Test
	public void findByTermDescriptionTest() {
		Term term = new Term("Spring 2021");
		termRepository.save(term);
		Term term1 = termRepository.findByDescription(term.getDescription());
		assertEquals(term.getDescription(), term1.getDescription());
	}

	// JUnit to add when Registration model is added
	@Test
	public void testSaveRegistration() {
		Term term = new Term("Spring 2021");

		Set<Registration> registrations = new HashSet<Registration>();

		Registration registration1 = new Registration("abc");
		registration1.setTerm(term);
		registrations.add(registration1);

		Registration registration2 = new Registration("def");
		registration2.setTerm(term);
		registrations.add(registration2);

		Registration registration3 = new Registration("ghi");
		registration3.setTerm(term);
		registrations.add(registration3);

		term.setRegistration(registrations);
		termRepository.save(term);
		Term term1 = termRepository.findByDescription("Spring 2021");
		assertNotNull(term);
		assertEquals(term.getDescription(), term1.getDescription());
		assertEquals(term.isDisabled(), term1.isDisabled());
		assertEquals(term1.getRegistration().size(), 3);

	}

}
