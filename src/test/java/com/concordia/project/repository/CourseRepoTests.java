package com.concordia.project.repository;
/**
 * CEJV 559 - Group 2 Project
 * Term repository units tests
 * Implementation with CRUD and pagination
 *
 * @author Elliott
 *
 */
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import com.concordia.project.model.Course;




@DataJpaTest
@RunWith(SpringRunner.class)
class CourseRepoTests {

	@Autowired
	private CourseRepository courseRepository;
	
	@Test
	public void testSaveCourse() {
		Course course = new Course("John", 0);
		Course course2 = courseRepository.save(course);
		
		assertThat(course2.getCourseId()).isGreaterThan(0);
	}
	
	@Test
	public void testDeleteCourse() {
		Course course = new Course("John", 0);
		courseRepository.save(course);
		courseRepository.delete(course);
	}
	
	@Test
	public void testSearchCourse() {
		
		Course course = new Course("John", 0);
		courseRepository.save(course);
		
		Course course2 = courseRepository.findByCourseCode("john");
		
		assertEquals(true, course2.getcourseCode().equalsIgnoreCase("john"));
	}
	
	@Test
	public void testUpdateCourse() {
		
		Course course = new Course("John", 0);
		courseRepository.save(course);
		
		Course course2 = courseRepository.findByCourseCode("john");
		
		course2.setCourseCode("bill");
		courseRepository.save(course2);
		
		Course course3 = courseRepository.findByCourseCode("bill");
		assertEquals(true, course3.getcourseCode().equalsIgnoreCase("bill"));
		
	}
	@Test
	public void paginationCoursesTest() {
		int totaltests = 100;
		for (int test = 0; test < totaltests; test++) {
			Course course = new Course("David" + test, 0);
			courseRepository.save(course);
		}
		

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());

		Page<Course> teamPageResult = courseRepository.findAll(paging);

		if (teamPageResult.hasContent()) {
			assertEquals(teamPageResult.getContent().size(), 10);
		}
	}


}
