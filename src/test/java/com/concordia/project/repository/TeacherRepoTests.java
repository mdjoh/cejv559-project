package com.concordia.project.repository;
/**
 * CEJV 559 - Group 2 Project
 * Teacher repository units tests
 * Implementation with CRUD and pagination
 *
 * @author Elliott
 *
 */
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import com.concordia.project.model.Teacher;


@DataJpaTest
@RunWith(SpringRunner.class)
class TeacherRepoTests {
	
	@Autowired
	private TeacherRepository teacherRepository;

	@Test
	public void testSaveTeacher() {
		Teacher teacher = new Teacher("John", "Wiggy", "john.smith@teacher.ca");
		Teacher teacher2 = teacherRepository.save(teacher);
		
		assertThat(teacher2.getTeacherId()).isGreaterThan(0);
	}
	
	@Test
	public void testDeleteTeacher() {
		Teacher teacher = new Teacher("John", "Wiggy", "john.smith@teacher.ca");
		teacherRepository.save(teacher);
		teacherRepository.delete(teacher);
	}
	
	@Test
	public void testSearchTeacher() {
		
		Teacher teacher = new Teacher("John", "Wiggy", "john.smith@teacher.ca");
		teacherRepository.save(teacher);
		
		Teacher teacher2 = teacherRepository.findByLastName("Wiggy");
		
		assertEquals(true, teacher2.getLastName().equalsIgnoreCase("Wiggy"));
	}
	
	@Test
	public void testUpdateTeacher() {
		
		Teacher teacher = new Teacher("John", "Wiggy", "john.smith@teacher.ca");
		teacherRepository.save(teacher);
		
		Teacher teacher2 = teacherRepository.findByLastName("Wiggy");
		
		teacher2.setLastName("Wiggy2");
		teacherRepository.save(teacher2);
		
		Teacher teacher3 = teacherRepository.findByLastName("Wiggy2");
		assertEquals(true, teacher3.getLastName().equalsIgnoreCase("Wiggy2"));
		
	}
	@Test
	public void paginationTeachersTest() {
		int totaltests = 100;
		for (int test = 0; test < totaltests; test++) {
			Teacher teacher = new Teacher("fname" + test, "lname" + test, "johncena@mail.com" + test);
			teacherRepository.save(teacher);
		}
		

		Pageable paging = PageRequest.of(1, 10, Sort.unsorted());

		Page<Teacher> teamPageResult = teacherRepository.findAll(paging);

		if (teamPageResult.hasContent()) {
			assertEquals(teamPageResult.getContent().size(), 10);
		}
	}

}
