package com.concordia.project.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * CEJV 559 - Group2 Project
 * Student is a model for the student table
 * Model has Student class with fields, constructors, getters, setters, toString
 */
/**
 * @author Tetiana
 */
@Entity
@Table(name = "student")
public class Student {

	private int studentId;
	
	@NotEmpty(message = "Student code cannot be empty")
	private String studentCode;
	
	@NotEmpty(message = "First name cannot be empty")
	@Size(min = 2, max = 35, message = "First name must be between 2 and 35 characters")
	private String firstName;
	
	@NotEmpty(message = "Last name cannot be empty")
	@Size(min = 2, max = 35, message = "Last name must be between 2 and 35 characters")
	private String lastName;
	
	@NotEmpty(message = "Student email cannot be empty")
	@Email(message = "Student email must be a valid email address")
	private String studentEmail;
	
	@Past(message = "Date of birth cannot be a future date")
	private Date dateOfBirth;
	
	@NotNull(message = "Status must be specified")
	private boolean disabled;
	
	// One to one mapping with student_image table
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "student_image_id", referencedColumnName = "student_image_id")
	private StudentImage studentImage;// = new StudentImage();

	// One to many mapping with registration table
	private Set<Registration> registration = new HashSet<Registration>();

	/**
	 * Empty constructor
	 */
	public Student() {
		// do nothing
	}

	/**
	 * Constructor that takes only mandatory columns
	 * 
	 * @param firstName
	 * @param lastName
	 * @param studentEmail
	 */
	public Student(String firstName, String lastName, String studentEmail) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentEmail = studentEmail;
		this.studentCode = generateCode();
	}

	/**
	 * Constructor that takes all columns such as mandatory and optional
	 * 
	 * @param studentCode
	 * @param firstName
	 * @param lastName
	 * @param studentEmail
	 * @param dateOfBirth
	 * @param disabled
	 * @param registration
	 */
	public Student(String studentCode, String firstName, String lastName, String studentEmail, Date dateOfBirth,
			boolean disabled, Set<Registration> registration) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentEmail = studentEmail;
		this.dateOfBirth = dateOfBirth;
		this.disabled = disabled;
		this.registration = registration;
		this.studentCode = generateCode();
	}

	// Getters, setters

	/**
	 * @return the studentId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "student_id")
	public int getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the studentCode
	 */
	@Column(name = "student_code", nullable = false)
	public String getStudentCode() {
		return studentCode;
	}

	/**
	 * @param studentCode the studentCode to set
	 */
	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	/**
	 * @return the firstName
	 */
	@Column(name = "first_name", nullable = false)
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	@Column(name = "last_name", nullable = false)
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the studentEmail
	 */
	@Column(name = "student_email", nullable = false)
	public String getStudentEmail() {
		return studentEmail;
	}

	/**
	 * @param studentEmail the studentEmail to set
	 */
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	/**
	 * @return the dateOfBirth
	 */
	@Column(name = "date_of_birth")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the disabled
	 */
	@Column(name = "disabled", nullable = false)
	public boolean getDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the registration
	 */
	@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Registration> getRegistration() {
		return registration;
	}

	/**
	 * @param registration the registration to set
	 */
	public void setRegistration(Set<Registration> registration) {
		this.registration = registration;
	}

	/**
	 * @return the auto generated studentCode
	 */
	private String generateCode() {
		return ("Student: " + this.studentId);
	}

	/**
	 * @return the studentImage
	 */
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "student_image_id")
	public StudentImage getStudentImage() {
		return studentImage;
	}

	/**
	 * @param studentImage the studentImage to set
	 */
	public void setStudentImage(StudentImage studentImage) {
		this.studentImage = studentImage;
	}

	/**
	 * toString method for Student class
	 * 
	 * @return All of the attributes of a Student object in one String
	 */
	@Override
	public String toString() {
		return "Student [studentId=" + studentId + ", studentCode=" + studentCode + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", studentEmail=" + studentEmail + ", dateOfBirth=" + dateOfBirth
				+ ", disabled=" + disabled + ", studentImage=" + studentImage + ", registration=" + registration + "]";
	}

}
