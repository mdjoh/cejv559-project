package com.concordia.project.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * CEJV 559 - Group2 Project
 * Term is a model for the term table
 * Model has Term class with fields, constructors, getters, setters, toString
 */
/**
 * @author Tetiana
 */
@Entity
@Table(name = "term")
public class Term {

	private int termId;
	
	@NotEmpty(message = "Term code cannot be empty")
	private String termCode;
	
	@NotEmpty(message = "Term code cannot be empty")
	private String description;
	
	@NotNull(message = "Status must be specified")
	private boolean disabled;

	// One to many mapping with registration table
	private Set<Registration> registration = new HashSet<Registration>();

	/**
	 * Empty constructor
	 */
	public Term() {
		// do nothing
	}

	/**
	 * Default constructor that takes only mandatory column
	 * 
	 * @param description
	 */
	public Term(String description) {
		super();
		this.description = description;
		this.termCode = generateCode();
		this.disabled = false;
	}
	
	/**
	 * Default constructor that takes all columns
	 * 
	 * @param termCode
	 * @param description
	 * @param disabled
	 * @param registration
	 */
	public Term(String termCode, String description, boolean disabled) {
		super();
		this.termCode = generateCode();
		this.description = description;
		this.disabled = disabled;
	}

	// Getters, setters

	/**
	 * @return the termId
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "term_id")
	public int getTermId() {
		return termId;
	}

	/**
	 * @param termId the termId to set
	 */
	public void setTermId(int termId) {
		this.termId = termId;
	}

	/**
	 * @return the termCode
	 */
	@Column(name = "term_code", nullable = false)
	public String getTermCode() {
		return termCode;
	}

	/**
	 * @param termCode the termCode to set
	 */
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	/**
	 * @return the description
	 */
	@Column(name = "description", nullable = false)
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		if (description.length() == 0) {
			this.description = description + "XXX";
		}

		else {
			this.description = description;
		}
	}

	/**
	 * @return the disabled
	 */
	@Column(name = "disabled", nullable = false)
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the registration
	 */
	@OneToMany(mappedBy = "term", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	public Set<Registration> getRegistration() {
		return registration;
	}

	/**
	 * @param registration the registration to set
	 */
	public void setRegistration(Set<Registration> registration) {
		this.registration = registration;
	}

	/**
	 * @return the auto generated registrationCode
	 */
	private String generateCode() {
		return (this.description.toLowerCase());
	}

	/**
	 * toString method for Term class
	 *
	 * @return All of the attributes of a Term object in one String
	 */
	@Override
	public String toString() {
		return "Term [termId=" + termId + ", termCode=" + termCode + ", description=" + description + ", disabled="
				+ disabled + ", registration=" + registration + "]";
	}

}
