package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.StudentImageDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.StudentImage;
import com.concordia.project.repository.StudentImageRepository;
import com.concordia.project.service.StudentImageService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project StudentImage Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Tetiana, Marchiano
 *
 */
@Service
public class StudentImageServiceImpl implements StudentImageService {

	@Autowired
	private StudentImageRepository studentImageRepository;

	// retrieve
	public StudentImageDTO getStudentImage(Integer studentImageId) throws ServiceException {
		Optional<StudentImage> optional = studentImageRepository.findById(studentImageId);
		StudentImage studentImage = null;
		if (optional.isPresent()) {
			studentImage = optional.get();
		} else {
			throw new ServiceException("StudentImage not found for id : " + studentImageId);
		}

		StudentImageDTO studentImageDTO = new StudentImageDTO();
		BeanUtils.copyProperties(studentImage, studentImageDTO);
		return studentImageDTO;
	}

	public List<StudentImageDTO> getAllStudentImages() {
		List<StudentImage> studentImages = this.studentImageRepository.findAll();

		List<StudentImageDTO> studentImageDTOs = new ArrayList<StudentImageDTO>(studentImages.size());
		for (StudentImage studentImage : studentImages) {
			StudentImageDTO studentImageDTO = new StudentImageDTO();
			BeanUtils.copyProperties(studentImage, studentImageDTO);
			studentImageDTOs.add(studentImageDTO);
		}

		return studentImageDTOs;
	}

	// create
	@Transactional
	public void saveStudentImage(StudentImageDTO studentImageDTO) throws ServiceException {
		if (studentImageDTO == null) {
			throw new ServiceException("StudentImage not saved");
		}
		StudentImage studentImage = new StudentImage();
		BeanUtils.copyProperties(studentImageDTO, studentImage);
		studentImageRepository.save(studentImage);
	}

	@Transactional
	public StudentImage saveStudentImageFile(StudentImageDTO studentImageDTO) throws ServiceException {
		StudentImage studentImage = new StudentImage();
		BeanUtils.copyProperties(studentImageDTO, studentImage);
		studentImageRepository.save(studentImage);
		return studentImage;
	}
	
	// delete
	@Transactional
	public void deleteStudentImage(Integer studentImageId) throws ServiceException {
		Optional<StudentImage> studentImageRetrieved = studentImageRepository.findById(studentImageId);

		if (studentImageRetrieved.isPresent()) {
			studentImageRepository.deleteById(studentImageId);
		}

		throw new ServiceException("Cannot Delete: StudentImage not found");
	}

	// update
	@Transactional
	public void updateStudentImage(StudentImageDTO studentImageDTO) throws ServiceException {
		Optional<StudentImage> optional = studentImageRepository.findById(studentImageDTO.getStudentImageId());
		StudentImage studentImage = null;
		if (optional.isPresent()) {
			studentImage = optional.get();
		} else {
			throw new ServiceException("StudentImage not found for id : " + studentImageDTO.getStudentImageId());
		}
		BeanUtils.copyProperties(studentImageDTO, studentImage);
		studentImageRepository.save(studentImage);

	}

	public Page<StudentImageDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<StudentImageDTO> studentImageDTOs = this.studentImageRepository.findAll(pageable)
				.map((studentImage -> DozerBeanMapperBuilder.buildDefault().map(studentImage, StudentImageDTO.class)));

		return studentImageDTOs;
	}

}
