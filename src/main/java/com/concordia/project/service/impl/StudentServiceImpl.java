package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.StudentDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Student;
import com.concordia.project.repository.StudentRepository;
import com.concordia.project.service.StudentService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project Student Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Tetiana
 *
 */
@Service
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentRepository studentRepository;

	// retrieve
	public StudentDTO getStudent(Integer studentId) throws ServiceException {
		Optional<Student> optional = studentRepository.findById(studentId);
		Student student = null;
		if (optional.isPresent()) {
			student = optional.get();
		} else {
			throw new ServiceException("Student not found for id : " + studentId);
		}

		StudentDTO studentDTO = new StudentDTO();
		BeanUtils.copyProperties(student, studentDTO);
		return studentDTO;
	}

	public List<StudentDTO> getAllStudents() {
		List<Student> students = this.studentRepository.findAll();

		List<StudentDTO> studentDTOs = new ArrayList<StudentDTO>(students.size());
		for (Student student : students) {
			StudentDTO studentDTO = new StudentDTO();
			BeanUtils.copyProperties(student, studentDTO);
			studentDTOs.add(studentDTO);
		}

		return studentDTOs;
	}

	// create
	@Transactional
	public void saveStudent(StudentDTO studentDTO) throws ServiceException {
		if (studentDTO == null) {
			throw new ServiceException("Student not saved");
		}
		Student student = new Student();
		BeanUtils.copyProperties(studentDTO, student);
		studentRepository.save(student);
	}

	// delete
	@Transactional
	public void deleteStudent(Integer studentId) throws ServiceException {
		Optional<Student> studentRetrieved = studentRepository.findById(studentId);

		if (studentRetrieved.isPresent()) {
			studentRepository.deleteById(studentId);
		}

		throw new ServiceException("Cannot Delete: Student not found");
	}

	// update
	@Transactional
	public void updateStudent(StudentDTO studentDTO) throws ServiceException {
		Optional<Student> optional = studentRepository.findById(studentDTO.getStudentId());
		Student student = null;
		if (optional.isPresent()) {
			student = optional.get();
		} else {
			throw new ServiceException("Student not found for id : " + studentDTO.getStudentId());
		}
		BeanUtils.copyProperties(studentDTO, student);
		studentRepository.save(student);

	}

	public Page<StudentDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<StudentDTO> studentDTOs = this.studentRepository.findAll(pageable)
				.map((student -> DozerBeanMapperBuilder.buildDefault().map(student, StudentDTO.class)));

		return studentDTOs;
	}

}
