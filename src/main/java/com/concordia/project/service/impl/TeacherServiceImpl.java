package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.TeacherDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Teacher;
import com.concordia.project.repository.TeacherRepository;
import com.concordia.project.service.TeacherService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project Teacher Service Layer Implementation
 * Implementation with CRUD and pagination
 * 
 * @author Xingming, Tetiana, Marchiano, Elliott
 *
 */
@Service
public class TeacherServiceImpl implements TeacherService {

	@Autowired
	private TeacherRepository teacherRepository;

	// retrieve
	public TeacherDTO getTeacher(Integer teacherId) throws ServiceException {
		Optional<Teacher> optional = teacherRepository.findById(teacherId);
		Teacher teacher = null;
		if (optional.isPresent()) {
			teacher = optional.get();
		} else {
			throw new ServiceException("Teacher not found for id : " + teacherId);
		}

		TeacherDTO teacherDTO = new TeacherDTO();
		BeanUtils.copyProperties(teacher, teacherDTO);
		return teacherDTO;
	}

	public List<TeacherDTO> getAllTeachers() {
		List<Teacher> teachers = this.teacherRepository.findAll();

		List<TeacherDTO> teacherDTOs = new ArrayList<TeacherDTO>(teachers.size());
		for (Teacher teacher : teachers) {
			TeacherDTO teacherDTO = new TeacherDTO();
			BeanUtils.copyProperties(teacher, teacherDTO);
			teacherDTOs.add(teacherDTO);
		}

		return teacherDTOs;
	}

	// create
	@Transactional
	public void saveTeacher(TeacherDTO teacherDTO) throws ServiceException {
		if (teacherDTO == null) {
			throw new ServiceException("Teacher not saved");
		}
		Teacher teacher = new Teacher();
		BeanUtils.copyProperties(teacherDTO, teacher);
		teacherRepository.save(teacher);
	}

	// delete
	@Transactional
	public void deleteTeacher(Integer teacherId) throws ServiceException {
		Optional<Teacher> teacherRetrieved = teacherRepository.findById(teacherId);

		if (teacherRetrieved.isPresent()) {
			teacherRepository.deleteById(teacherId);
		}

		throw new ServiceException("Cannot Delete: Teacher not found");
	}

	// update
	@Transactional
	public void updateTeacher(TeacherDTO teacherDTO) throws ServiceException {
		Optional<Teacher> optional = teacherRepository.findById(teacherDTO.getTeacherId());
		Teacher teacher = null;
		if (optional.isPresent()) {
			teacher = optional.get();
		} else {
			throw new ServiceException("Teacher not found for id : " + teacherDTO.getTeacherId());
		}
		BeanUtils.copyProperties(teacherDTO, teacher);
		teacherRepository.save(teacher);

	}

	public Page<TeacherDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<TeacherDTO> teacherDTOs = this.teacherRepository.findAll(pageable)
				.map((teacher -> DozerBeanMapperBuilder.buildDefault().map(teacher, TeacherDTO.class)));

		return teacherDTOs;
	}

}
