package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.DepartmentDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Department;
import com.concordia.project.repository.DepartmentRepository;
import com.concordia.project.service.DepartmentService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project Department Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Elliott
 *
 */
@Service
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentRepository departmentRepository;

	// retrieve
	public DepartmentDTO getDepartment(Integer departmentId) throws ServiceException {
		Optional<Department> optional = departmentRepository.findById(departmentId);
		Department department = null;
		if (optional.isPresent()) {
			department = optional.get();
		} else {
			throw new ServiceException("Department not found for id : " + departmentId);
		}

		DepartmentDTO departmentDTO = new DepartmentDTO();
		BeanUtils.copyProperties(department, departmentDTO);
		return departmentDTO;
	}

	public List<DepartmentDTO> getAllDepartments() {
		List<Department> departments = this.departmentRepository.findAll();

		List<DepartmentDTO> departmentDTOs = new ArrayList<DepartmentDTO>(departments.size());
		for (Department department : departments) {
			DepartmentDTO departmentDTO = new DepartmentDTO();
			BeanUtils.copyProperties(department, departmentDTO);
			departmentDTOs.add(departmentDTO);
		}

		return departmentDTOs;
	}

	// create
	@Transactional
	public void saveDepartment(DepartmentDTO departmentDTO) throws ServiceException {
		if (departmentDTO == null) {
			throw new ServiceException("Department not saved");
		}
		Department department = new Department();
		BeanUtils.copyProperties(departmentDTO, department);
		departmentRepository.save(department);
	}

	// delete
	@Transactional
	public void deleteDepartment(Integer departmentId) throws ServiceException {
		Optional<Department> departmentRetrieved = departmentRepository.findById(departmentId);

		if (departmentRetrieved.isPresent()) {
			departmentRepository.deleteById(departmentId);
		}

		throw new ServiceException("Cannot Delete: Department not found");
	}

	// update
	@Transactional
	public void updateDepartment(DepartmentDTO departmentDTO) throws ServiceException {
		Optional<Department> optional = departmentRepository.findById(departmentDTO.getDepartmentId());
		Department department = null;
		if (optional.isPresent()) {
			department = optional.get();
		} else {
			throw new ServiceException("Department not found for id : " + departmentDTO.getDepartmentId());
		}
		BeanUtils.copyProperties(departmentDTO, department);
		departmentRepository.save(department);

	}

	public Page<DepartmentDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<DepartmentDTO> departmentDTOs = this.departmentRepository.findAll(pageable)
				.map((department -> DozerBeanMapperBuilder.buildDefault().map(department, DepartmentDTO.class)));

		return departmentDTOs;
	}

}
