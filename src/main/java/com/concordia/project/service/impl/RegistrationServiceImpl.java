package com.concordia.project.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.dto.RegistrationDTO;
import com.concordia.project.dto.StudentDTO;
import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.Course;
import com.concordia.project.model.Registration;
import com.concordia.project.model.Student;
import com.concordia.project.model.Term;
import com.concordia.project.repository.CourseRepository;
import com.concordia.project.repository.RegistrationRepository;
import com.concordia.project.repository.StudentRepository;
import com.concordia.project.repository.TermRepository;
import com.concordia.project.service.RegistrationService;
import com.github.dozermapper.core.DozerBeanMapperBuilder;

/**
 * CEJV 559 - Group 2 Project Registration Service Layer Implementation
 * Implementation with CRUD and pagination
 *
 * @author Tetiana, Elliott
 *
 */
@Service
public class RegistrationServiceImpl implements RegistrationService {

	@Autowired
	private RegistrationRepository registrationRepository;

	@Autowired
	private StudentRepository studentRepository;

	@Autowired
	private CourseRepository courseRepository;

	@Autowired
	private TermRepository termRepository;

	// retrieve
	public RegistrationDTO getRegistration(Integer registrationId) throws ServiceException {
		Optional<Registration> optional = registrationRepository.findById(registrationId);
		Registration registration = null;
		if (optional.isPresent()) {
			registration = optional.get();
		} else {
			throw new ServiceException("Registration not found for id : " + registrationId);
		}

		RegistrationDTO registrationDTO = new RegistrationDTO();
		BeanUtils.copyProperties(registration, registrationDTO);
		return registrationDTO;
	}

	public List<RegistrationDTO> getAllRegistrations() {
		List<Registration> registrations = this.registrationRepository.findAll();

		List<RegistrationDTO> registrationDTOs = new ArrayList<RegistrationDTO>(registrations.size());
		for (Registration registration : registrations) {
			RegistrationDTO registrationDTO = new RegistrationDTO();
			BeanUtils.copyProperties(registration, registrationDTO);
			registrationDTOs.add(registrationDTO);
		}

		return registrationDTOs;
	}

	// create
	@Transactional
	public void saveRegistration(RegistrationDTO registrationDTO, StudentDTO studentDTO, CourseDTO courseDTO, TermDTO termDTO) throws ServiceException {
		if (registrationDTO == null) {
			throw new ServiceException("Registration not saved");
		}
		Registration registration = new Registration();
		Student student = studentRepository.getOne(studentDTO.getStudentId());
		Course course = courseRepository.getOne(courseDTO.getCourseId());
		Term term = termRepository.getOne(termDTO.getTermId());
		registration.setStudent(student);
		registration.setCourse(course);
		registration.setTerm(term);
		BeanUtils.copyProperties(registrationDTO, registration);
		registrationRepository.save(registration);
	}

	// delete
	@Transactional
	public void deleteRegistration(Integer registrationId) {
			registrationRepository.deleteById(registrationId);
	}

	// update
	@Transactional
	public void updateRegistration(RegistrationDTO registrationDTO) throws ServiceException {
		Optional<Registration> optional = registrationRepository.findById(registrationDTO.getRegistrationId());
		Registration registration = null;
		if (optional.isPresent()) {
			registration = optional.get();
		} else {
			throw new ServiceException("Registration not found for id : " + registrationDTO.getRegistrationId());
		}
		BeanUtils.copyProperties(registrationDTO, registration);
		registrationRepository.save(registration);

	}

	public Page<RegistrationDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection) {
		Sort sort = sortDirection.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortField).ascending()
				: Sort.by(sortField).descending();

		Pageable pageable = PageRequest.of(pageNo - 1, pageSize, sort);

		Page<RegistrationDTO> registrationDTOs = this.registrationRepository.findAll(pageable)
				.map((registration -> DozerBeanMapperBuilder.buildDefault().map(registration, RegistrationDTO.class)));

		return registrationDTOs;
	}

	// retrieve corresponding student
	public StudentDTO getRegistrationStudent(Integer registrationId) throws ServiceException {
		Optional<Registration> optional = registrationRepository.findById(registrationId);
		Registration registration = null;
		if (optional.isPresent()) {
			registration = optional.get();
		} else {
			throw new ServiceException("Registration not found for id : " + registrationId);
		}

		Student student = registration.getStudent();

		Optional<Student> optionalStudent = studentRepository.findById(student.getStudentId());
		Student student1 = null;
		if (optional.isPresent()) {
			student1 = optionalStudent.get();
		} else {
			throw new ServiceException("Student not found");
		}
		StudentDTO studentDTO = new StudentDTO();
		BeanUtils.copyProperties(student1, studentDTO);
		return studentDTO;
	}

	// retrieve corresponding course
	public CourseDTO getRegistrationCourse(Integer registrationId) throws ServiceException {
		Optional<Registration> optional = registrationRepository.findById(registrationId);
		Registration registration = null;
		if (optional.isPresent()) {
			registration = optional.get();
		} else {
			throw new ServiceException("Registration not found for id : " + registrationId);
		}

		Course course = registration.getCourse();

		Optional<Course> optionalCourse = courseRepository.findById(course.getCourseId());
		Course course1 = null;
		if (optional.isPresent()) {
			course1 = optionalCourse.get();
		} else {
			throw new ServiceException("Course not found");
		}
		CourseDTO courseDTO = new CourseDTO();
		BeanUtils.copyProperties(course1, courseDTO);
		return courseDTO;
	}

	// retrieve corresponding term
	public TermDTO getRegistrationTerm(Integer registrationId) throws ServiceException {
		Optional<Registration> optional = registrationRepository.findById(registrationId);
		Registration registration = null;
		if (optional.isPresent()) {
			registration = optional.get();
		} else {
			throw new ServiceException("Registration not found for id : " + registrationId);
		}

		Term term = registration.getTerm();

		Optional<Term> optionalTerm = termRepository.findById(term.getTermId());
		Term term1 = null;
		if (optional.isPresent()) {
			term1 = optionalTerm.get();
		} else {
			throw new ServiceException("Term not found");
		}
		TermDTO termDTO = new TermDTO();
		BeanUtils.copyProperties(term1, termDTO);
		return termDTO;
	}
}