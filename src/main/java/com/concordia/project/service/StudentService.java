package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.StudentDTO;
import com.concordia.project.exception.ServiceException;

/**
 * CEJV 559 - Group 2 Project
 * StudentService is a service layer for the student table
 * Service has StudentDTO interface with create, retrieve, update and delete methods for Student DTOs
 */
/**
 * @author Tetiana
 */
@Service
public interface StudentService {
	
	/**
	 * Get a student by it's ID
	 * 
	 * @param studentId
	 * @return Student DTO with specified student ID
	 */
	StudentDTO getStudent(Integer studentId) throws ServiceException;
	
	/**
	 * Get multiple students
	 * 
	 * @return Collection of Students DTOs
	 */
	List<StudentDTO> getAllStudents() throws ServiceException;

	/**
	 * Create and save a new Student DTO
	 * 
	 * @param studentDTO An object of StudentDTO class
	 */
	void saveStudent(StudentDTO studentDTO) throws ServiceException;

	/**
	 * Delete a Student
	 * 
	 * @param studentId The Student ID
	 */
	void deleteStudent(Integer studentId) throws ServiceException;

	/**
	 * Update a Student
	 * 
	 * @param studentCode The student code to update with
	 * @param studentDTO  An object of StudentDTO class
	 */
	void updateStudent(StudentDTO studentDTO) throws ServiceException;

	/**
	 * Paginate StudentDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<StudentDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
