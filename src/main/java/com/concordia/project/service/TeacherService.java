package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.TeacherDTO;
import com.concordia.project.exception.ServiceException;

/**
 * CEJV 559 - Group 2 Project
 * TeacherService is a service layer for the teacher table
 * Service has TeacherDTO interface with create, retrieve, update and delete methods for Teacher DTOs
 */
/**
 * @author Tetiana, Xingming, Marchiano, Elliott
 */

@Service
public interface TeacherService {

	/**
	 * Get a teacher by it's ID
	 * 
	 * @param teacherId
	 * @return Teacher DTO with specified teacher ID
	 */
	TeacherDTO getTeacher(Integer teacherId) throws ServiceException;

	/**
	 * Get multiple teachers
	 * 
	 * @return Collection of Teachers DTOs
	 */
	List<TeacherDTO> getAllTeachers() throws ServiceException;

	/**
	 * Create and save a new Teacher DTO
	 * 
	 * @param teacherDTO An object of TeacherDTO class
	 */
	void saveTeacher(TeacherDTO teacherDTO) throws ServiceException;

	/**
	 * Delete a Teacher
	 * 
	 * @param teacherId The Teacher ID
	 */
	void deleteTeacher(Integer teacherId) throws ServiceException;

	/**
	 * Update a Teacher
	 * 
	 * @param teacherCode The teacher code to update with
	 * @param teacherDTO  An object of TeacherDTO class
	 */
	void updateTeacher(TeacherDTO teacherDTO) throws ServiceException;

	/**
	 * Paginate TeacherDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<TeacherDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);
}
