package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.DepartmentDTO;
import com.concordia.project.exception.ServiceException;

/**
 * CEJV 559 - Group 2 Project
 * DepartmentService is a service layer for the department table
 * Service has DepartmentDTO interface with create, retrieve, update and delete methods for Department DTOs
 */
/**
 * @author Elliott
 */

@Service
public interface DepartmentService {
	/**
	 * Get a department by it's ID
	 * 
	 * @param departmentId
	 * @return Department DTO with specified department ID
	 */
	DepartmentDTO getDepartment(Integer departmentId) throws ServiceException;

	/**
	 * Get multiple departments
	 * 
	 * @return Collection of Departments DTOs
	 */
	List<DepartmentDTO> getAllDepartments() throws ServiceException;

	/**
	 * Create and save a new Department DTO
	 * 
	 * @param departmentDTO An object of DepartmentDTO class
	 */
	void saveDepartment(DepartmentDTO departmentDTO) throws ServiceException;

	/**
	 * Delete a Department
	 * 
	 * @param departmentId The Department ID
	 */
	void deleteDepartment(Integer departmentId) throws ServiceException;

	/**
	 * Update a Department
	 * 
	 * @param departmentCode The department code to update with
	 * @param departmentDTO  An object of DepartmentDTO class
	 */
	void updateDepartment(DepartmentDTO departmentDTO) throws ServiceException;

	/**
	 * Paginate DepartmentDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<DepartmentDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}