package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.exception.ServiceException;

/**
 * CEJV 559 - Group 2 Project
 * CourseService is a service layer for the course table
 * Service has CourseDTO interface with create, retrieve, update and delete methods for Course DTOs
 */
/**
 * @author Elliott
 */

@Service
public interface CourseService {
	/**
	 * Get a course by it's ID
	 * 
	 * @param courseId
	 * @return Course DTO with specified course ID
	 */
	CourseDTO getCourse(Integer courseId) throws ServiceException;
	
	/**
	 * Get multiple courses
	 * 
	 * @return Collection of Courses DTOs
	 */
	List<CourseDTO> getAllCourses() throws ServiceException;

	/**
	 * Create and save a new Course DTO
	 * 
	 * @param courseDTO An object of CourseDTO class
	 */
	void saveCourse(CourseDTO courseDTO) throws ServiceException;

	/**
	 * Delete a Course
	 * 
	 * @param courseId The Course ID
	 */
	void deleteCourse(Integer courseId) throws ServiceException;

	/**
	 * Update a Course
	 * 
	 * @param courseCode The course code to update with
	 * @param courseDTO  An object of CourseDTO class
	 */
	void updateCourse(CourseDTO courseDTO) throws ServiceException;

	/**
	 * Paginate CourseDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<CourseDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
