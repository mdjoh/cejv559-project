package com.concordia.project.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;

/**
 * CEJV 559 - Group 2 Project
 * TermService is a service layer for the term table
 * Service has TermDTO interface with create, retrieve, update and delete methods for Term DTOs
 */
/**
 * @author Tetiana
 */
@Service
public interface TermService {
	
	/**
	 * Get a term by it's ID
	 * 
	 * @param termId
	 * @return Term DTO with specified term ID
	 */
	TermDTO getTerm(Integer termId) throws ServiceException;
	
	/**
	 * Get multiple terms
	 * 
	 * @return Collection of Terms DTOs
	 */
	List<TermDTO> getAllTerms() throws ServiceException;

	/**
	 * Create and save a new Term DTO
	 * 
	 * @param termDTO An object of TermDTO class
	 */
	void saveTerm(TermDTO termDTO) throws ServiceException;

	/**
	 * Delete a Term
	 * 
	 * @param termId The Term ID
	 */
	void deleteTerm(Integer termId) throws ServiceException;

	/**
	 * Update a Term
	 * 
	 * @param termCode The term code to update with
	 * @param termDTO  An object of TermDTO class
	 */
	void updateTerm(TermDTO termDTO) throws ServiceException;

	/**
	 * Paginate TermDTO records
	 * 
	 * @param pageNo    Page Number, integer
	 * @param pageSize  Page Size, integer
	 * @param sortField Sorting table field, String
	 * @param sortDir   Sorting type/direction, String
	 */
	Page<TermDTO> findPaginated(int pageNo, int pageSize, String sortField, String sortDirection);

}
