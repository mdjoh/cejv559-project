package com.concordia.project.repository;
/**
 * CEJV 559 - Group 2 Project
 * Repository for teacher table
 * Teacher Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Elliott
 */
import org.springframework.stereotype.Repository;

import com.concordia.project.model.Department;

@Repository
public interface DepartmentRepository extends org.springframework.data.jpa.repository.JpaRepository<Department, Integer> {


	/**
	 * 
	 * @param departmentCode
	 * @return
	 */
	Department findByDepartmentCode(String departmentCode);
	

	/**
	 * Find term by specified description
	 * 
	 * @param description
	 * @return Department object with specified description
	 */
	Department findByDescription(String description);

}
