package com.concordia.project.repository;

import org.springframework.stereotype.Repository;

import com.concordia.project.model.StudentImage;

/**
 * CEJV 559 - Group 2 Project
 * Repository for student_image table
 * Course Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Tetiana
 */
@Repository
public interface StudentImageRepository extends org.springframework.data.jpa.repository.JpaRepository<StudentImage, Integer>{
	
	/**
	 * Find student image by specified code
	 * 
	 * @param studentImageCode
	 * @return StudentImage object with specified code
	 */
	StudentImage findByStudentImageCode(String studentImageCode);

}
