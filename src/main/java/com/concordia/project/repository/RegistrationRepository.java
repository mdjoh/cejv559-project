package com.concordia.project.repository;

import org.springframework.stereotype.Repository;

import com.concordia.project.model.Registration;

/**
 * CEJV 559 - Group 2 Project
 * Repository for registration table
 * Course Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Tetiana
 */
@Repository
public interface RegistrationRepository
					extends org.springframework.data.jpa.repository.JpaRepository<Registration, Integer>{
	
	/**
	 * Find registration by specified code
	 * 
	 * @param registrationCode
	 * @return Course object with specified code
	 */
	Registration findByRegistrationCode(String registrationCode);

}
