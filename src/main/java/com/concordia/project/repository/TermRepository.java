package com.concordia.project.repository;

import org.springframework.stereotype.Repository;

import com.concordia.project.model.Term;

/**
 * CEJV 559 - Group 2 Project
 * Repository for term table
 * Course Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Tetiana
 */
@Repository
public interface TermRepository extends org.springframework.data.jpa.repository.JpaRepository<Term, Integer> {

	/**
	 * Find term by specified code
	 * 
	 * @param termCode
	 * @return Term object with specified code
	 */
	Term findByTermCode(String termCode);
	
	/**
	 * Find term by specified description
	 * 
	 * @param description
	 * @return Term object with specified description
	 */
	Term findByDescription(String description);

}
