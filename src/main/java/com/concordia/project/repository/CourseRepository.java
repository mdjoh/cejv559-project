package com.concordia.project.repository;
/**
 * CEJV 559 - Group 2 Project
 * Repository for teacher table
 * Teacher Repository CRUD is responsible to Create, Read, Update, Delete, count, list, pagination, sorting
 */
/**
 * @author Elliott
 */
import org.springframework.stereotype.Repository;

import com.concordia.project.model.Course;

@Repository
public interface CourseRepository extends org.springframework.data.jpa.repository.JpaRepository<Course, Integer>{
	
	
	/**
	 * Find course by Id
	 * @param courseId
	 * @return
	 */
	Course findByCourseId(int courseId);
	
	/**
	 * Find course by code
	 * @param courseCode
	 * @return
	 */
	Course findByCourseCode(String courseCode);
	 

}
