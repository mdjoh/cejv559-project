package com.concordia.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.TermService;

/**
 * CEJV 559 - Group 2 Project
 * Controller for Term
 * /**
 *
 * @author Marchiano, Tetiana
 */

@Controller
public class TermController {

	@Autowired
	private TermService termService;

	/**
	 * Page to display records
	 * @param model
	 * @return page of records sorted by termId
	 */
	@GetMapping("/terms")
	public String viewHomePage(Model model)  throws ServiceException {
		return findPaginated(1, "termId", "asc", model);
	}

	/**
	 * Add new term form page
	 * @param model
	 * @return new term page which has the form to add new term 
	 */
	@GetMapping({ "/showNewTermForm" })
	public String showNewTermForm(Model model) {
		// create model attribute to bind form data
		TermDTO  termDTO = new TermDTO();
		model.addAttribute("term", termDTO);
		return "new_term";
	}

	/**
	 * Save term
	 * @param termDTO
	 * @return page with term records with new term added
	 * @throws ServiceException if term cannot be saved
	 */
	@PostMapping("/saveTerm")
	public String saveTerm(@ModelAttribute("term") TermDTO termDTO) throws ServiceException {
		termService.saveTerm(termDTO);
		return "redirect:/terms";
	}
	
	/**
	 * Update term
	 * @param termId
	 * @param model
	 * @return update term page which has the form to update existing term
	 * @throws ServiceException if term cannot be updated
	 */
	@GetMapping("/showUpdateTermForm/{termId}")
	public String showUpdateTermForm(@PathVariable ( value = "termId") int termId, Model model) throws ServiceException {
		
		TermDTO termDTO = termService.getTerm(termId);
		
		// set term as a model attribute to pre-populate the form
		model.addAttribute("term", termDTO);
		return "update_term";
	}
	
	/**
	 * Delete term
	 * @param termId
	 * @return page with term records with term of interest deleted
	 * @throws ServiceException if term cannot be deleted
	 */
	@GetMapping("/deleteTerm/{termId}")
	public String deleteTerm(@PathVariable (value = "termId") int termId) throws ServiceException {
		
		this.termService.deleteTerm(termId);
		return "redirect:/terms";
	}

	/**
	 * Pagination for terms page
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated term records page
	 */
	@GetMapping("pageTerm/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		
		int pageSize = 5;
		
		Page<TermDTO> page = termService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<TermDTO> listTerms = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listTerm", listTerms);
		return "terms";

	}
}
