package com.concordia.project.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.dto.RegistrationDTO;
import com.concordia.project.dto.StudentDTO;
import com.concordia.project.dto.TermDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.CourseService;
import com.concordia.project.service.RegistrationService;
import com.concordia.project.service.StudentService;
import com.concordia.project.service.TermService;

/**
 * CEJV 559 - Group 2 Project Controller for Registration /**
 *
 * @author Marchiano, Tetiana
 */

@Controller
public class RegistrationController {

	@Autowired
	private RegistrationService registrationService;

	@Autowired
	private StudentService studentService;

	@Autowired
	private CourseService courseService;

	@Autowired
	private TermService termService;

	/**
	 * Page to display records
	 * 
	 * @param model
	 * @return page of records sorted by registrationId
	 */
	@GetMapping("/registrations")
	public String viewHomePage(Model model) throws ServiceException {
		return findPaginated(1, "registrationId", "asc", model);
	}

	/**
	 * Add new registration form page
	 * 
	 * @param model
	 * @return new registration page which has the form to add new registration
	 * @throws ServiceException
	 */
	@GetMapping({ "/showNewRegistrationForm" })
	public String showNewRegistrationForm(Model model) throws ServiceException {
		// create model attribute to bind form data
		RegistrationDTO registrationDTO = new RegistrationDTO();
		model.addAttribute("registration", registrationDTO);

		List<StudentDTO> students = studentService.getAllStudents();
		model.addAttribute("students", students);
		StudentDTO student = students.get(0);
		model.addAttribute("student", student);

		List<CourseDTO> courses = courseService.getAllCourses();
		model.addAttribute("courses", courses);
		CourseDTO course = courses.get(0);
		model.addAttribute("course", course);

		List<TermDTO> terms = termService.getAllTerms();
		model.addAttribute("terms", terms);
		TermDTO term = terms.get(0);
		model.addAttribute("term", term);

		return "new_registration";
	}

	/**
	 * Save registration
	 * 
	 * @param registrationDTO
	 * @return page with registration records with new registration added
	 * @throws ServiceException if registration cannot be saved
	 */
	@PostMapping("/saveRegistration")
	public String saveRegistration(@ModelAttribute("registration") RegistrationDTO registrationDTO, StudentDTO studentDTO, CourseDTO courseDTO, TermDTO termDTO)
			throws ServiceException {
		registrationService.saveRegistration(registrationDTO, studentDTO, courseDTO, termDTO);
		return "redirect:/registrations";
	}

	/**
	 * Update registration
	 * 
	 * @param registrationId
	 * @param model
	 * @return update registration page which has the form to update existing
	 *         registration
	 * @throws ServiceException if registration cannot be updated
	 */
	@GetMapping("/showUpdateRegistrationForm/{registrationId}")
	public String showUpdateRegistrationForm(@PathVariable(value = "registrationId") int registrationId, Model model)
			throws ServiceException {

		RegistrationDTO registrationDTO = registrationService.getRegistration(registrationId);

		// set registration as a model attribute to pre-populate the form
		model.addAttribute("registration", registrationDTO);
		// showing the corresponding student
		StudentDTO student = registrationService.getRegistrationStudent(registrationId);
		model.addAttribute("student", student);
		// showing the corresponding course
		CourseDTO course = registrationService.getRegistrationCourse(registrationId);
		model.addAttribute("course", course);
		// showing the corresponding term
		TermDTO term = registrationService.getRegistrationTerm(registrationId);
		model.addAttribute("term", term);
		// showing the list of students available
		List<StudentDTO> students = studentService.getAllStudents();
		model.addAttribute("students", students); 
		// showing the list of courses available
		List<CourseDTO> courses = courseService.getAllCourses();
		model.addAttribute("courses", courses); 
		// showing the list of terms available
		List<TermDTO> terms = termService.getAllTerms();
		model.addAttribute("terms", terms); 
		
		
		return "update_registration";
	}

	/**
	 * Delete registration
	 * 
	 * @param registrationId
	 * @return page with registration records with registration of interest deleted
	 * @throws ServiceException if registration cannot be deleted
	 */
	@GetMapping("/deleteRegistration/{registrationId}")
	public String deleteRegistration(@PathVariable(value = "registrationId") int registrationId)
			throws ServiceException {

		this.registrationService.deleteRegistration(registrationId);
		return "redirect:/registrations";
	}

	/**
	 * Pagination for registrations page
	 * 
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated registration records page
	 */
	@GetMapping("pageRegistration/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo, @RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir, Model model) {

		int pageSize = 5;

		Page<RegistrationDTO> page = registrationService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<RegistrationDTO> listRegistrations = page.getContent();

		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());

		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

		model.addAttribute("listRegistration", listRegistrations);
		return "registrations";

	}
}
