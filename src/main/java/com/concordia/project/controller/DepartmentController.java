package com.concordia.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.concordia.project.dto.DepartmentDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.DepartmentService;

/**
 * CEJV 559 - Group 2 Project Controller for Department /**
 *
 * @author Marchiano
 */

@Controller
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	/**
	 * Page to display department records
	 * 
	 * @param model
	 * @return page of records sorted by departmentId
	 */
	@GetMapping("/departments")
	public String viewHomePage(Model model) throws ServiceException {
		return findDepartmentPaginated(1, "departmentId", "asc", model);
	}

	/**
	 * Add new department form page
	 * 
	 * @param model
	 * @return new department page which has the form to add new department
	 */
	@GetMapping({ "/showNewDepartmentForm" })
	public String showNewDepartmentForm(Model model) {
		// create model attribute to bind form data
		DepartmentDTO departmentDTO = new DepartmentDTO();
		model.addAttribute("department", departmentDTO);
		return "new_department";
	}

	/**
	 * Save department
	 * 
	 * @param departmentDTO
	 * @return page with department records with new department added
	 * @throws ServiceException if department cannot be saved
	 */
	@PostMapping("/saveDepartment")
	public String saveDepartment(@ModelAttribute("department") DepartmentDTO departmentDTO) throws ServiceException {
		departmentService.saveDepartment(departmentDTO);
		return "redirect:/departments";
	}

	/**
	 * Update department
	 * 
	 * @param departmentId
	 * @param model
	 * @return update department page which has the form to update existing
	 *         department
	 * @throws ServiceException if department cannot be updated
	 */
	@GetMapping("/showUpdateDepartmentForm/{departmentId}")
	public String showUpdateDepartmentForm(@PathVariable(value = "departmentId") int departmentId, Model model)
			throws ServiceException {

		DepartmentDTO departmentDTO = departmentService.getDepartment(departmentId);

		// set department as a model attribute to pre-populate the form
		model.addAttribute("department", departmentDTO);
		return "update_department";
	}

	/**
	 * Delete department
	 * 
	 * @param departmentId
	 * @return page with department records with department of interest deleted
	 * @throws ServiceException if department cannot be deleted
	 */
	@GetMapping("/deleteDepartment/{departmentId}")
	public String deleteDepartment(@PathVariable(value = "departmentId") int departmentId) throws ServiceException {

		this.departmentService.deleteDepartment(departmentId);
		return "redirect:/departments";
	}

	/**
	 * Pagination for departments page
	 * 
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated department records page
	 */
	@GetMapping("pageDepartment/{pageNo}")
	public String findDepartmentPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		
		int pageSize = 5;
		
		Page<DepartmentDTO> page = departmentService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<DepartmentDTO> listDepartments = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listDepartment", listDepartments);
		return "departments";

	}
}
