package com.concordia.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.concordia.project.dto.StudentDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.StudentService;

/**
 * CEJV 559 - Group 2 Project Controller for Student /**
 *
 * @author Marchiano
 */

@Controller
public class StudentController {

	@Autowired
	private StudentService studentService;

	/**
	 * Page to display records
	 * 
	 * @param model
	 * @return page of records sorted by studentId
	 */
	@GetMapping("/students")
	public String viewHomePage(Model model) throws ServiceException {
		return findPaginated(1, "studentId", "asc", model);
	}

	/**
	 * Add new student form page
	 * 
	 * @param model
	 * @return new student page which has the form to add new student
	 */
	@GetMapping({ "/showNewStudentForm" })
	public String showNewStudentForm(Model model) {
		// create model attribute to bind form data
		StudentDTO studentDTO = new StudentDTO();
		model.addAttribute("student", studentDTO);
		return "new_student";
	}

	/**
	 * Save student
	 * 
	 * @param studentDTO
	 * @return page with student records with new student added
	 * @throws ServiceException if student cannot be saved
	 */
	@PostMapping("/saveStudent")
	public String saveStudent(@ModelAttribute("student") StudentDTO studentDTO) throws ServiceException {
		studentService.saveStudent(studentDTO);
		return "redirect:/students";
	}

	/**
	 * Update student
	 * 
	 * @param studentId
	 * @param model
	 * @return update student page which has the form to update existing student
	 * @throws ServiceException if student cannot be updated
	 */
	@GetMapping("/showUpdateStudentForm/{studentId}")
	public String showUpdateStudentForm(@PathVariable(value = "studentId") int studentId, Model model)
			throws ServiceException {

		StudentDTO studentDTO = studentService.getStudent(studentId);

		// set student as a model attribute to pre-populate the form
		model.addAttribute("student", studentDTO);
		return "update_student";
	}

	/**
	 * Delete student
	 * 
	 * @param studentId
	 * @return page with student records with student of interest deleted
	 * @throws ServiceException if student cannot be deleted
	 */
	@GetMapping("/deleteStudent/{studentId}")
	public String deleteStudent(@PathVariable(value = "studentId") int studentId) throws ServiceException {

		this.studentService.deleteStudent(studentId);
		return "redirect:/students";
	}

	/**
	 * Pagination for students page
	 * 
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated student records page
	 */
	@GetMapping("pageStudent/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, @RequestParam("sortDir") String sortDir, Model model) {

		int pageSize = 5;

		Page<StudentDTO> page = studentService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<StudentDTO> listStudents = page.getContent();

		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());

		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

		model.addAttribute("listStudent", listStudents);
		return "students";

	}

}
