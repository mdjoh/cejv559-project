package com.concordia.project.controller;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.concordia.project.dto.StudentImageDTO;
import com.concordia.project.exception.ImageException;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.model.StudentImage;
import com.concordia.project.service.StudentImageService;

/**
 * CEJV 559 - Group 2 Project Controller for StudentImage /**
 *
 * @author Marchiano, Tetiana
 */

@Controller
public class StudentImageController {

	@Autowired
	private StudentImageService studentImageService;

	/**
	 * Page to display records
	 * 
	 * @param model
	 * @return page of records sorted by studentImageId
	 */
	@GetMapping("/student_images")
	public String viewHomePage(Model model) throws ServiceException {
		return findPaginated(1, "studentImageId", "asc", model);
	}

	/**
	 * Add new student image form page
	 * 
	 * @param model
	 * @return new student image page which has the form to add new student image
	 */
	@GetMapping({ "/showNewStudentImageForm" })
	public String showNewStudentImageForm(Model model) {
		// create model attribute to bind form data
		StudentImageDTO studentImageDTO = new StudentImageDTO();
		model.addAttribute("studentImage", studentImageDTO);
		return "new_student_image";
	}

	/**
	 * Save student image
	 * 
	 * @param studentImageDTO
	 * @return page with student image records with new student image added
	 * @throws ServiceException if student image cannot be saved
	 * @throws IOException 
	 * @throws ImageException
	 */
	@PostMapping("/saveStudentImage")
	public String saveStudentImage(@ModelAttribute("studentImage") StudentImageDTO studentImageDTO,
			@RequestParam("imageFile") MultipartFile multipartFile) throws ServiceException, IOException, ImageException {

		String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		studentImageDTO.setStudentImageFile(fileName);
		
		StudentImage savedStudentImage = studentImageService.saveStudentImageFile(studentImageDTO);
		
		String uploadDir = "./student-images/" + savedStudentImage.getStudentImageId();
		Path uploadPath = Paths.get(uploadDir);
        
        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
         
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {        
            throw new ImageException("Could not save image file: " + fileName + " to database");     
        }
        
        return "redirect:/student_images";
	}

	/**
	 * Update student image
	 * 
	 * @param studentImageId
	 * @param model
	 * @return update student image page which has the form to update existing
	 *         student image
	 * @throws ServiceException if student image cannot be updated
	 */
	@GetMapping("/showUpdateStudentImageForm/{studentImageId}")
	public String showUpdateStudentImageForm(@PathVariable(value = "studentImageId") int studentImageId, Model model)
			throws ServiceException {

		StudentImageDTO studentImageDTO = studentImageService.getStudentImage(studentImageId);

		// set studentImage as a model attribute to pre-populate the form
		model.addAttribute("studentImage", studentImageDTO);
		return "update_student_image";
	}

	/**
	 * Delete student image
	 * 
	 * @param studentImageId
	 * @return page with student image records with student image of interest
	 *         deleted
	 * @throws ServiceException if student image cannot be deleted
	 */
	@GetMapping("/deleteStudentImage/{studentImageId}")
	public String deleteStudentImage(@PathVariable(value = "studentImageId") int studentImageId)
			throws ServiceException {

		this.studentImageService.deleteStudentImage(studentImageId);
		return "redirect:/student_images";
	}

	/**
	 * Pagination for student_images page
	 * 
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated student image records page
	 */
	@GetMapping("pageStudentImage/{pageNo}")
	public String findPaginated(@PathVariable(value = "pageNo") int pageNo,
			@RequestParam("sortField") String sortField, @RequestParam("sortDir") String sortDir, Model model) {

		int pageSize = 5;

		Page<StudentImageDTO> page = studentImageService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<StudentImageDTO> listStudentImages = page.getContent();

		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());

		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");

		model.addAttribute("listStudentImage", listStudentImages);
		return "student_images";

	}

}
