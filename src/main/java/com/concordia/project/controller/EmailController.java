package com.concordia.project.controller;

import javax.mail.MessagingException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.concordia.project.dto.CourseDTO;
import com.concordia.project.email.model.ContactForm;
import com.concordia.project.email.service.EmailService;
import com.concordia.project.exception.EmailException;
import com.concordia.project.service.CourseService;

/**
 * CEJV 559 - Group 2 Project Controller for Email /**
 *
 * @author Tetiana
 */
@Controller
public class EmailController {
	
	@Autowired
	private EmailService emailService;

	/**
	 * Show contact page
	 * 
	 * @param model
	 * @return contact page
	 */
	@GetMapping(value = "/contact")
	public String contactUs(Model model) {
		ContactForm contactForm = new ContactForm();
		model.addAttribute("contactForm", contactForm);
		return "contact";
	}
	
	/**
	 * Show contact page
	 * 
	 * @param model
	 * @return email_sent page
	 */
	@PostMapping(value = "/emailSent")
	public String emailSent(@ModelAttribute("contactForm") ContactForm contactForm) throws MessagingException, EmailException {
		emailService.contactUs(contactForm);
		return "email_sent";
	}


}
