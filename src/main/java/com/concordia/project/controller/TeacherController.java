package com.concordia.project.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.concordia.project.dto.TeacherDTO;
import com.concordia.project.exception.ServiceException;
import com.concordia.project.service.TeacherService;

/**
 * CEJV 559 - Group 2 Project
 * Controller for Teacher
 * /**
 *
 * @author Xingming, Tetiana, Elliott, Marchiano
 */

@Controller
public class TeacherController {

	@Autowired
	private TeacherService teacherService;

	/**
	 * Page to display records
	 * @param model
	 * @return page of records sorted by teacherId
	 */
	@GetMapping("/teachers")
	public String viewHomePage(Model model)  throws ServiceException{
		return findPaginated(1, "teacherId", "asc", model);
	}

	/**
	 * Add new teacher form page
	 * @param model
	 * @return new teacher page which has the form to add new teacher 
	 */
	@GetMapping({ "/showNewTeacherForm" })
	public String showNewTeacherForm(Model model) {
		// create model attribute to bind form data
		TeacherDTO  teacherDTO = new TeacherDTO();
		model.addAttribute("teacher", teacherDTO);
		return "new_teacher";
	}

	/**
	 * Save teacher
	 * @param teacherDTO
	 * @return page with teacher records with new teacher added
	 * @throws ServiceException if teacher cannot be saved
	 */
	@PostMapping("/saveTeacher")
	public String saveTeacher(@ModelAttribute("teacher") TeacherDTO teacherDTO) throws ServiceException {
		teacherService.saveTeacher(teacherDTO);
		return "redirect:/teachers";
	}
	
	/**
	 * Update teacher
	 * @param teacherId
	 * @param model
	 * @return update teacher page which has the form to update existing teacher
	 * @throws ServiceException if teacher cannot be updated
	 */
	@GetMapping("/showUpdateTeacherForm/{teacherId}")
	public String showUpdateTeacherForm(@PathVariable ( value = "teacherId") int teacherId, Model model) throws ServiceException {
		
		TeacherDTO teacherDTO = teacherService.getTeacher(teacherId);
		
		// set teacher as a model attribute to pre-populate the form
		model.addAttribute("teacher", teacherDTO);
		return "update_teacher";
	}
	
	/**
	 * Delete teacher
	 * @param teacherId
	 * @return page with teacher records with teacher of interest deleted
	 * @throws ServiceException if teacher cannot be deleted
	 */
	@GetMapping("/deleteTeacher/{teacherId}")
	public String deleteTeacher(@PathVariable (value = "teacherId") int teacherId) throws ServiceException {

		this.teacherService.deleteTeacher(teacherId);
		return "redirect:/teachers";
	}

	/**
	 * Pagination for teachers page
	 * @param pageNo
	 * @param sortField
	 * @param sortDir
	 * @param model
	 * @return paginated teacher records page
	 */
	@GetMapping("pageTeacher/{pageNo}")
	public String findPaginated(@PathVariable (value = "pageNo") int pageNo, 
			@RequestParam("sortField") String sortField,
			@RequestParam("sortDir") String sortDir,
			Model model) {
		
		int pageSize = 5;
		
		Page<TeacherDTO> page = teacherService.findPaginated(pageNo, pageSize, sortField, sortDir);
		List<TeacherDTO> listTeachers = page.getContent();
		
		model.addAttribute("currentPage", pageNo);
		model.addAttribute("totalPages", page.getTotalPages());
		model.addAttribute("totalItems", page.getTotalElements());
		
		model.addAttribute("sortField", sortField);
		model.addAttribute("sortDir", sortDir);
		model.addAttribute("reverseSortDir", sortDir.equals("asc") ? "desc" : "asc");
		
		model.addAttribute("listTeacher", listTeachers);
		return "teachers";

	}

}
