package com.concordia.project.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.concordia.project.model.User;
import com.concordia.project.repository.UserRepository;

/**
 * CEJV 559 - Group 2 Project
 * Home Controller - Controller to display index page
 * 
 * @author Marchiano Tetiana Xingming
 */
@Controller
public class HomeController {
	
	@Autowired
	private UserRepository userRepo;

	/**
	 * Show index page
	 * @param model
	 * @return index page
	 */
	@GetMapping("/")
	public String viewHomePage(Model model) {
		return "index";
	}
	
	/**
	 * Show about page
	 * @param model
	 * @return about page
	 */
	@GetMapping("/about")
	public String viewAboutPage(Model model) {
		return "about";
	}
	
	/**
	 * Show about page
	 * @param model
	 * @return about page
	 */
	@GetMapping("/signup")
	public String viewSignUpPage(Model model) {
		model.addAttribute("user", new User());
		return "user_signUp";
	}
	
	/**
	 * User info registration processor
	 * @param user
	 * @return
	 */
	@PostMapping("/process_register")
	public String processRegister(User user) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		String encodedPassword = passwordEncoder.encode(user.getPassword());
		user.setPassword(encodedPassword);
		
		userRepo.save(user);
		
		return "register_success";
	}
	
	/**
	 * log in error controller
	 * @return
	 */
	@GetMapping("/index")
	public String showLoginPage() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication == null || authentication instanceof AnonymousAuthenticationToken) {
			return "index";
		}
		
		return "redirect:/";
		
		
	}
	
}
