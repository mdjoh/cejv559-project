package com.concordia.project.config;

import java.nio.file.Path;
import java.nio.file.Paths;
 
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/** CEJV 559 - Group 2 Project
* MVC Configuration for student images in database
* adapted from https://www.codejava.net/frameworks/spring-boot/spring-boot-file-upload-tutorial
* 
* @author Marchiano
*
*/

@Configuration
public class MvcConfig implements WebMvcConfigurer {
 
 
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    	Path uploadDir = Paths.get("./student-images");
        String uploadPath = uploadDir.toFile().getAbsolutePath();
        
        registry.addResourceHandler("/student-images/**").addResourceLocations("file:/"+ uploadPath + "/");
    }
}
