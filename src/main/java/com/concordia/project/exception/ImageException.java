package com.concordia.project.exception;

public class ImageException extends Exception {

	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -6852768609975772363L;

	public ImageException(String errorMessage) {
		super(errorMessage);
	}
}