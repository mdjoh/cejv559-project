package com.concordia.project.exception;
/**
 * Email Exception handles every errors for email exceptions.
 *
 * @author Tetiana
 */
public class EmailException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public EmailException(String errorMessage) {
        super(errorMessage);
    }

}
