package com.concordia.project.email.model;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * CEJV 559 - Group2 Project
 * ContactForm is a model for the contact us fields
 * implementing Email Service
 */
/**
 * @author Tetaiana
 */

public class ContactForm {
	
	@NotEmpty(message = "Name cannot be empty")
	@Size(min = 2, max = 35, message = "Name must be between 2 and 35 characters")
	private String emailName;
	
	@NotEmpty(message = "Sender cannot be empty")
	@Size(min = 2, max = 35, message = "Sender must be between 2 and 35 characters")
	private String emailSender;
	
	@NotNull(message = "Message cannot be null")
	private String emailMessage;
	
	/**
	 * Empty constructor
	 */
	public ContactForm(){
		// do nothing
	}

	/**
	 * @param emailName
	 * @param emailSender
	 * @param emailMessage
	 */
	public ContactForm(String emailName, String emailSender, String emailMessage) {
		super();
		this.emailName = emailName;
		this.emailSender = emailSender;
		this.emailMessage = emailMessage;
	}

	/**
	 * @return the emailName
	 */
	public String getEmailName() {
		return emailName;
	}

	/**
	 * @param emailName the emailName to set
	 */
	public void setEmailName(String emailName) {
		this.emailName = emailName;
	}

	/**
	 * @return the emailSender
	 */
	public String getEmailSender() {
		return emailSender;
	}

	/**
	 * @param emailSender the emailSender to set
	 */
	public void setEmailSender(String emailSender) {
		this.emailSender = emailSender;
	}

	/**
	 * @return the emailMessage
	 */
	public String getEmailMessage() {
		return emailMessage;
	}

	/**
	 * @param emailMessage the emailMessage to set
	 */
	public void setEmailMessage(String emailMessage) {
		this.emailMessage = emailMessage;
	}

	@Override
	public String toString() {
		return "ContactForm [emailName=" + emailName + ", emailSender=" + emailSender + ", emailMessage=" + emailMessage
				+ "]";
	}
	
	
	
}
