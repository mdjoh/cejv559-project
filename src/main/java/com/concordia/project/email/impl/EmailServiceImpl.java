package com.concordia.project.email.impl;

import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.concordia.project.email.model.ContactForm;
import com.concordia.project.email.service.EmailService;
import com.concordia.project.exception.EmailException;

/**
 * CEJV 559 - Group 2 Project
 * Service implementation for Email
 * /**
 *
 * @author Tetiana
 */

@Component
public class EmailServiceImpl implements EmailService{
	
	public boolean sendEmail(String emailAddressFrom, String emailAddressTo, String subject, String messageContent)
			throws EmailException {

		try {		
			JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
			mailSender.setHost("smtp.mail.yahoo.com");
	        mailSender.setPort(587);
	          
	        mailSender.setUsername("xingmingsitu");
	        mailSender.setPassword("ppjysbgowctgasmy");
	          
	        Properties props = mailSender.getJavaMailProperties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.debug", "true");
	        props.put("mail.smtp.ssl.trust", "smtp.mail.yahoo.com");

			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);

			helper.setSubject(subject);
			helper.setFrom(emailAddressFrom);
			helper.setTo(emailAddressTo);

			boolean html = true;
			helper.setText(messageContent, html);

			mailSender.send(message);

			return true;
		} catch (MailException | MessagingException e) {
			throw new EmailException(e.getMessage());
		}
	}
	
	public boolean contactUs(ContactForm contactForm)
			throws EmailException {
		String emailAddressFrom = "xingmingsitu@yahoo.com";
		String emailAddressTo = contactForm.getEmailSender();
		String subject = "Contact form message";
		String messageContent = "Hello! \n We've just received your message:\n" + contactForm.getEmailMessage() + "\nThank you, " + contactForm.getEmailName() + ", we will contact you soon";

		try {		
			JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
			mailSender.setHost("smtp.mail.yahoo.com");
	        mailSender.setPort(587);
	          
	        mailSender.setUsername("xingmingsitu");
	        mailSender.setPassword("ppjysbgowctgasmy");
	          
	        Properties props = mailSender.getJavaMailProperties();
	        props.put("mail.transport.protocol", "smtp");
	        props.put("mail.smtp.auth", "true");
	        props.put("mail.smtp.starttls.enable", "true");
	        props.put("mail.debug", "true");
	        props.put("mail.smtp.ssl.trust", "smtp.mail.yahoo.com");

			MimeMessage message = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(message);

			helper.setSubject(subject);
			helper.setFrom(emailAddressFrom);
			helper.setTo(emailAddressTo);

			boolean html = true;
			helper.setText(messageContent, html);

			mailSender.send(message);

			return true;
		} catch (MailException | MessagingException e) {
			throw new EmailException(e.getMessage());
		}
		
	}

}
