package com.concordia.project.dto;

import java.io.Serializable;

/**
 * CEJV 559 - Group2 Project
 * TermDTO is a model for the term table
 * Model has TermDTO class with fields, constructors, getters, setters, hashCode, equals and toString
 */
/**
 * @author Tetiana
 */
public class TermDTO implements Serializable{

	/**
	 * serialVersion
	 */
	private static final Long serialVersionUID = 1L;
	
	private int termId;
	private String termCode;
	private String description;
	private boolean disabled;
	
	/**
	 * Empty constructor
	 */
	public TermDTO() {
		// do nothing
	}

	/**
	 * @param termId
	 * @param termCode
	 * @param description
	 * @param disabled
	 */
	public TermDTO(int termId, String termCode, String description, boolean disabled) {
		super();
		this.termId = termId;
		this.termCode = generateCode();
		this.description = description;
		this.disabled = disabled;
	}

	// Getters, setters
	/**
	 * @return the termId
	 */
	public int getTermId() {
		return termId;
	}

	/**
	 * @param termId the termId to set
	 */
	public void setTermId(int termId) {
		this.termId = termId;
	}

	/**
	 * @return the termCode
	 */
	public String getTermCode() {
		return termCode;
	}

	/**
	 * @param termCode the termCode to set
	 */
	public void setTermCode(String termCode) {
		this.termCode = termCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the auto generated registrationCode
	 */
	private String generateCode() {
		return (this.description.toLowerCase());
	}

	// hash code and equals methods
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((termCode == null) ? 0 : termCode.hashCode());
		result = prime * result + (int) (termId ^ (termId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TermDTO other = (TermDTO) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (disabled != other.disabled)
			return false;
		if (termCode == null) {
			if (other.termCode != null)
				return false;
		} else if (!termCode.equals(other.termCode))
			return false;
		if (termId != other.termId)
			return false;
		return true;
	}

	/**
	 * toString method for TermDTO class
	 * 
	 * @return All of the attributes of a TermDTO object in one String
	 */
	@Override
	public String toString() {
		return "TermDTO [termId=" + termId + ", termCode=" + termCode + ", description=" + description + ", disabled="
				+ disabled + "]";
	}
	
	
}
