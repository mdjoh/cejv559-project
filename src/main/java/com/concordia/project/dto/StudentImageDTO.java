package com.concordia.project.dto;

import java.io.Serializable;

import javax.persistence.Transient;

/**
 * CEJV 559 - Group2 Project
 * StudentImageDTO is a model for the student table
 * Model has StudentImageDTO class with fields, constructors, getters, setters, hashCode, equals and toString
 */
/**
 * @author Tetiana, Marchiano
 */
public class StudentImageDTO implements Serializable {
	/**
	 * serialVersion
	 */
	private static final Long serialVersionUID = 1L;
	
	private int studentImageId;
	private String studentImageCode;
	private String studentImageFile;
	
	/**
	 * Empty constructor
	 */
	public StudentImageDTO() {
		// do nothing
	}

	/**
	 * Default constructor
	 * @param studentImageId
	 * @param studentImageCode
	 * @param studentImageFile
	 */
	public StudentImageDTO(int studentImageId, String studentImageCode, String studentImageFile) {
		super();
		this.studentImageId = studentImageId;
		this.studentImageCode = studentImageCode;
		this.studentImageFile = studentImageFile;
	}

	// Getters, setters
	/**
	 * @return the studentImageId
	 */
	public int getStudentImageId() {
		return studentImageId;
	}

	/**
	 * @param studentImageId the studentImageId to set
	 */
	public void setStudentImageId(int studentImageId) {
		this.studentImageId = studentImageId;
	}

	/**
	 * @return the studentImageCode
	 */
	public String getStudentImageCode() {
		return studentImageCode;
	}

	/**
	 * @param studentImageCode the studentImageCode to set
	 */
	public void setStudentImageCode(String studentImageCode) {
		this.studentImageCode = studentImageCode;
	}
	
	/**
	 * @return the studentImageFile
	 */
	public String getStudentImageFile() {
		return studentImageFile;
	}

	/**
	 * @param studentImageFile the studentImageFile to set
	 */
	public void setStudentImageFile(String studentImageFile) {
		this.studentImageFile = studentImageFile;
	}

	/**
	 * @return student image path
	 */
	@Transient
	public String getStudentImageFileImagePath() {
		if (studentImageFile == null)
			return null;

		return "/student-images/" + studentImageId + "/" + studentImageFile;
	}
	
	// hash code and equals methods
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((studentImageCode == null) ? 0 : studentImageCode.hashCode());
		result = prime * result + ((studentImageFile == null) ? 0 : studentImageFile.hashCode());
		result = prime * result + studentImageId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentImageDTO other = (StudentImageDTO) obj;
		if (studentImageCode == null) {
			if (other.studentImageCode != null)
				return false;
		} else if (!studentImageCode.equals(other.studentImageCode))
			return false;
		if (studentImageFile == null) {
			if (other.studentImageFile != null)
				return false;
		} else if (!studentImageFile.equals(other.studentImageFile))
			return false;
		if (studentImageId != other.studentImageId)
			return false;
		return true;
	}

	/**
	 * toString method for StudentImageDTO class
	 * 
	 * @return All of the attributes of a StudentImageDTO object in one String
	 */
	@Override
	public String toString() {
		return "StudentImageDTO [studentImageId=" + studentImageId + ", studentImageFile="
				+ studentImageFile + ", studentImageCode=" + studentImageCode + "]";
	}

}
