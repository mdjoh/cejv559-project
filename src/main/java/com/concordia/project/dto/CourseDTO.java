package com.concordia.project.dto;

import java.io.Serializable;

/**
 * CEJV 559 - Group2 Project
 * CourseDTO is a model for the course table
 * Model has CourseDTO class with fields, constructors, getters, setters, toString
 */
/**
 * @author Elliott
 */

public class CourseDTO implements Serializable {

	/**
	 * serialVersionUID
	 */
	private static final Long serialVersionUID = 1L;
	private int courseId;
	private String courseCode;
	private String description;
	private boolean disabled;
	private double courseCost;

	/**
	 * Empty constructor
	 */
	public CourseDTO() {
		// do nothing
	}

	/**
	 * @param courseId
	 * @param courseCode
	 * @param description
	 * @param disabled
	 * @param courseCost
	 */
	public CourseDTO(int courseId, String courseCode, String description, boolean disabled, double courseCost) {
		super();
		this.courseId = courseId;
		this.courseCode = generateCode();
		this.description = description;
		this.setCourseCost(courseCost);
		this.disabled = disabled;
	}

	// Getters, setters, and toString

	/**
	 * @return the courseId
	 */
	public int getCourseId() {
		return courseId;
	}

	/**
	 * @param courseId the courseId to set
	 */
	public void setCourseId(int courseId) {
		this.courseId = courseId;
	}

	/**
	 * @return the courseCode
	 */
	public String getCourseCode() {
		return courseCode;
	}

	/**
	 * @param courseCode the courseCode to set
	 */
	public void setCourseCode(String courseCode) {
		this.courseCode = courseCode;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}

	/**
	 * @return the courseCost
	 */
	public double getCourseCost() {
		return courseCost;
	}

	/**
	 * @param courseCost the courseCost to set
	 */
	public void setCourseCost(double courseCost) {
		if (courseCost < 0) {

			courseCost = 0.0;
		} else {
			this.courseCost = courseCost;
		}
	}

	/**
	 * @return the auto generated registrationCode
	 */
	private String generateCode() {
		return (this.description.toLowerCase());
	}

	// hash code and equals methods

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((courseCode == null) ? 0 : courseCode.hashCode());
		long temp;
		temp = Double.doubleToLongBits(courseCost);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + courseId;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + (disabled ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CourseDTO other = (CourseDTO) obj;
		if (courseCode == null) {
			if (other.courseCode != null)
				return false;
		} else if (!courseCode.equals(other.courseCode))
			return false;
		if (Double.doubleToLongBits(courseCost) != Double.doubleToLongBits(other.courseCost))
			return false;
		if (courseId != other.courseId)
			return false;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (disabled != other.disabled)
			return false;
		return true;
	}

	/**
	 * toString method for CourseDTO class
	 * 
	 * @return All of the attributes of a CourseDTO object in one String
	 */
	@Override
	public String toString() {
		return "CourseDTO [courseId=" + courseId + ", courseCode=" + courseCode + ", description=" + description
				+ ", disabled=" + disabled + ", courseCost=" + courseCost + "]";
	}

}
