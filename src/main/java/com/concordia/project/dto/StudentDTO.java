package com.concordia.project.dto;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

/**
 * CEJV 559 - Group2 Project
 * StudentDTO is a model for the student table
 * Model has StudentDTO class with fields, constructors, getters, setters, hashCode, equals and toString
 */
/**
 * @author Tetiana
 */
public class StudentDTO implements Serializable{

	/**
	 * serialVersion
	 */
	private static final Long serialVersionUID = 1L;

	private int studentId;
	private String studentCode;
	private String firstName;
	private String lastName;
	private String studentEmail;
	private Date dateOfBirth;
	private boolean disabled;
	
	/**
	 * Empty constructor
	 */
	public StudentDTO() {
		// do nothing
	}

	/**
	 * Default constructor
	 * @param studentId
	 * @param studentCode
	 * @param firstName
	 * @param lastName
	 * @param studentEmail
	 * @param dateOfBirth
	 * @param disabled
	 */
	public StudentDTO(int studentId, String studentCode, String firstName, String lastName, String studentEmail,
			Date dateOfBirth, boolean disabled) {
		super();
		this.studentId = studentId;
		this.studentCode = generateCode();
		this.firstName = firstName;
		this.lastName = lastName;
		this.studentEmail = studentEmail;
		this.dateOfBirth = dateOfBirth;
		this.disabled = disabled;
	}

	// Getters, setters
	/**
	 * @return the studentId
	 */
	public int getStudentId() {
		return studentId;
	}

	/**
	 * @param studentId the studentId to set
	 */
	public void setStudentId(int studentId) {
		this.studentId = studentId;
	}

	/**
	 * @return the studentCode
	 */
	public String getStudentCode() {
		return studentCode;
	}

	/**
	 * @param studentCode the studentCode to set
	 */
	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the studentEmail
	 */
	public String getStudentEmail() {
		return studentEmail;
	}

	/**
	 * @param studentEmail the studentEmail to set
	 */
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	/**
	 * @return the dateOfBirth
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	/**
	 * @param dateOfBirth the dateOfBirth to set
	 */
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	/**
	 * @return the disabled
	 */
	public boolean isDisabled() {
		return disabled;
	}

	/**
	 * @param disabled the disabled to set
	 */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	/**
	 * @return the auto generated studentCode
	 */
	private String generateCode() {
		return ("Student: " + this.studentId);
	}

	// hash code and equals methods
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((studentCode == null) ? 0 : studentCode.hashCode());
		result = prime * result + ((studentEmail == null) ? 0 : studentEmail.hashCode());
		result = prime * result + (int) (studentId ^ (studentId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		StudentDTO other = (StudentDTO) obj;
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null)
				return false;
		} else if (!dateOfBirth.equals(other.dateOfBirth))
			return false;
		if (disabled != other.disabled)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (studentCode == null) {
			if (other.studentCode != null)
				return false;
		} else if (!studentCode.equals(other.studentCode))
			return false;
		if (studentEmail == null) {
			if (other.studentEmail != null)
				return false;
		} else if (!studentEmail.equals(other.studentEmail))
			return false;
		if (studentId != other.studentId)
			return false;
		return true;
	}

	/**
	 * toString method for StudentDTO class
	 * 
	 * @return All of the attributes of a StudentDTO object in one String
	 */
	@Override
	public String toString() {
		return "StudentDTO [studentId=" + studentId + ", studentCode=" + studentCode + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", studentEmail=" + studentEmail + ", dateOfBirth=" + dateOfBirth
				+ ", disabled=" + disabled + "]";
	}
	
	
}
