package com.concordia.project.dto;

import java.io.Serializable;

/**
 * CEJV 559 Final Project
 * Teacher Data Transfer Object (DTO) to display information to frontend and to interact with backend
 */
/**
 * @author Elliott
 */

public class TeacherDTO implements Serializable {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 1L;
	
	private int teacherId;
	private String teacherCode;
	private String firstName;
	private String lastName;
	private String teacherEmail;
	private boolean disabled;
	
	/**
	 * Empty constructor
	 */
	public TeacherDTO() {
		// do nothing
	}
	
	/**
	 * Default constructor for TeacherDTO
	 * @param teacherId
	 * @param teacherCode
	 * @param firstName the first name of the teacher
	 * @param lastName the last name of the teacher
	 * @param teacherEmail
	 * @param disabled the status of the teacher
	 */
	public TeacherDTO(String firstName, String lastName, String teacherEmail, boolean disabled)
	{
		
		this.teacherCode = generateCode();
		this.firstName = firstName;
		this.lastName = lastName;
		this.teacherEmail = teacherEmail;
		this.disabled = disabled;
	}
	
	private String generateCode() {

		return ("Teacher-" + this.teacherId);
	}
	
	// Getters and setters
	/**
	 * @return the teacher ID
	 */
	public int getTeacherId() {
		return teacherId;
	}
	
	
	
	
	/**
	 * @param teacherId the teacherId to set
	 */
	public void setTeacherId(int teacherId) {
		this.teacherId = teacherId;
	}

	/**
	 * @return the code of the teacher
	 */
	public String getTeacherCode() {
		return teacherCode;
	}
	
	
	
	
    /**
	 * @param teacherCode the teacherCode to set
	 */
	public void setTeacherCode(String teacherCode) {
		this.teacherCode = teacherCode;
	}

	/**
     * @return the first name of the teacher
     */
	public String getFirstName() {
		return firstName;
	}
    /**
     * Set the first name of the teacher
     * @param firstName
     */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
    /**
     * @return the last name of the teacher
     */
	public String getLastName() {
		return lastName;
	}
    /**
     * Set the last name of the teacher
     * @param lastName
     */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
    /**
     * @return teacher's email
     */
	public String getTeacherEmail() {
		return teacherEmail;
	}
    /**
     * Set the email of the teacher
     * @param teacherEmail
     */
	public void setTeacherEmail(String teacherEmail) {
		this.teacherEmail = teacherEmail;
	}
	
    /**
     * @return teacher's status
     */
	public boolean getDisabled() {
		return disabled;
	}
    /**
     * Set teacher's status
     * @param disabled
     */
	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
	
	// hash code method
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (disabled ? 1231 : 1237);
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((teacherCode == null) ? 0 : teacherCode.hashCode());
		result = prime * result + ((teacherEmail == null) ? 0 : teacherEmail.hashCode());
		result = prime * result + (int) (teacherId ^ (teacherId >>> 32));
		return result;
	}
	
	// equals method
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TeacherDTO other = (TeacherDTO) obj;
		if (disabled != other.disabled)
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (teacherCode == null) {
			if (other.teacherCode != null)
				return false;
		} else if (!teacherCode.equals(other.teacherCode))
			return false;
		if (teacherEmail == null) {
			if (other.teacherEmail != null)
				return false;
		} else if (!teacherEmail.equals(other.teacherEmail))
			return false;
		if (teacherId != other.teacherId)
			return false;
		return true;
	}
	
    /**
     * toString method for Teacher DTO class
     * @return All of the attributes of a Teacher DTO in one String
     */
	@Override
	public String toString() {
		return "TeacherDTO [teacherId=" + teacherId + ", teacherCode=" + teacherCode + ", firstName=" + firstName
				+ ", lastName=" + lastName + ", teacherEmail=" + teacherEmail + ", disabled=" + disabled + "]";
	}
}
