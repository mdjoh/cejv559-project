DROP TABLE IF EXISTS student_image;

CREATE TABLE student_image(
student_image_id INT AUTO_INCREMENT  PRIMARY KEY,
student_image_code VARCHAR(50),
student_image_file VARCHAR(50)

);

DROP TABLE IF EXISTS student;

CREATE TABLE student(
  student_id INT AUTO_INCREMENT  PRIMARY KEY,
  student_code VARCHAR(50),
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  student_email VARCHAR(50),
  date_of_birth DATE,
  student_image_fk INT,
  disabled boolean DEFAULT 0
);

ALTER TABLE student
ADD CONSTRAINT FK_image FOREIGN KEY (student_image_fk)     
      REFERENCES student_image(student_image_id);




DROP TABLE IF EXISTS department;

CREATE TABLE department(
  department_id INT AUTO_INCREMENT  PRIMARY KEY,
  description VARCHAR(50),
  disabled boolean DEFAULT 0
  
);


DROP TABLE IF EXISTS teacher;

CREATE TABLE teacher(
  teacher_id INT AUTO_INCREMENT  PRIMARY KEY,
  teacher_code VARCHAR(50),
  first_name VARCHAR(50),
  last_name VARCHAR(50),
  teacher_email VARCHAR(50),
  department_fk INT,
  disabled boolean DEFAULT 0
);

ALTER TABLE teacher
ADD CONSTRAINT FK_department_teacher FOREIGN KEY (department_fk)     
      REFERENCES department(department_id);

DROP TABLE IF EXISTS course;

CREATE TABLE course(
  course_id INT AUTO_INCREMENT  PRIMARY KEY,
  teacher_fk INT,
  description VARCHAR(50),
  department_fk VARCHAR(50),
  cost DOUBLE precision,
  disabled boolean DEFAULT 0
    
);

ALTER TABLE course
ADD CONSTRAINT FK_course_teacher FOREIGN KEY (teacher_fk)     
      REFERENCES teacher(teacher_id);



DROP TABLE IF EXISTS term;

CREATE TABLE term(
  term_id INT AUTO_INCREMENT  PRIMARY KEY,
  description VARCHAR(50),
  disabled boolean DEFAULT 0
  
);



DROP TABLE IF EXISTS registration;

CREATE TABLE registration(
  registration_id INT AUTO_INCREMENT  PRIMARY KEY,
  student_fk INT,
  term_fk INT,
  course_fk INT
  
);

ALTER TABLE registration
ADD CONSTRAINT FK_student_reg FOREIGN KEY (student_fk)     
      REFERENCES student(student_id);
	  
ALTER TABLE registration
ADD CONSTRAINT FK_term_reg FOREIGN KEY (term_fk)     
      REFERENCES term(term_id);

ALTER TABLE registration
ADD CONSTRAINT FK_course_reg FOREIGN KEY (course_fk)     
      REFERENCES course(course_id);

