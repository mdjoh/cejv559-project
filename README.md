# CEJV559-Final Project Group 2

This is the CEJV 559 Final Project Repository of Group 2.

Our project is a web application for a school management system. It involves registration, departments, teachers, terms, courses, and students. A school administrator is able to create, retrieve, update, and delete records pertaining to the school through our application.

Our project used tools including:
- Java 11
- Spring
- Spring Boot 2.4.3
- Spring MVC
- JPA
- Hibernate 5
- JUnit
- Maven 4.0.0
- MySQL 8.0.23
- H2 Database
- Thymeleaf
- HTML5
- CSS3
- Bootstrap 4.0.0

## MySQL Instructions
MySQL Install (adapted from [1](https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04)):

Step 1 — Installing MySQL
On Ubuntu 20.1, you can install MySQL using the APT package repository.

$ sudo apt update

Then install the mysql-server package:
$ sudo apt-get install mysql-server


Step 2 — Configuring MySQL\
For fresh installations of MySQL, you’ll want to run the DBMS’s included security script. This script changes some of the less secure default options for things like remote root logins and sample users.

Run the security script with sudo:
$ sudo mysql_secure_installation

This will take you through a series of prompts where you can make some changes to your MySQL installation’s security options. The first prompt will ask whether you’d like to set up the Validate Password Plugin, which can be used to test the password strength of new MySQL users before deeming them valid.

If you elect to set up the Validate Password Plugin, any MySQL user you create that authenticates with a password will be required to have a password that satisfies the policy you select.

The next prompt will be to set a password for the MySQL root user. Enter and then confirm a secure password of your choice.

If you used the Validate Password Plugin, you’ll receive feedback on the strength of your new password. If you’re satisfied with the strength of the password you just entered, enter Y to continue the script.

Step 3 - Configuring a User "concordia"

It is best practice not to use the root account for regular use.

$ sudo mysql

then:

mysql> CREATE USER 'concordia'@'localhost' IDENTIFIED BY 'password';

mysql> GRANT ALL PRIVILEGES ON *.* TO 'concordia'@'localhost' WITH GRANT OPTION;

Step 4 - Free up memory from previous operations

mysql> FLUSH PRIVILEGES;
mysql> exit

Step 5 - Testing MySQL

$ sudo mysql -u concordia -p

Enter the password configued above.

$ exit

To see if MySQL is running:

$ sudo systemctl status mysql.service

To get out of the output, press Ctrl+c

If MySQL isn’t running, you can start it with:

$ sudo systemctl start mysql

Step 6 - Create the database

$ sudo mysql -u concordia -p

Enter the password for this user.

mysql> create database test1;\
mysql> use test1;

mysql> exit

Step 7 - Importing the Schema and Test Data

Copy the schemadata.sql into the current directory.

Execute the command:

$ mysql -u root -p test1 < schemadata.sql

The schema and data should be imported.

## Internationalization Notice
In application.properties, line 48 "spring.messages.encoding=ISO-8859-1" is needed if you use Spring Boot Suite. If you use Eclipse, you do not need this line.

## Entity Relationship Diagram
![School Database Entity Relationship Diagram](src/main/resources/static/images/ERD.png "School Database Entity Relationship Diagram")

## Sitemap
![School Sitemap](src/main/resources/static/images/sitemap.PNG "School Sitemap")
---
## Sources
1: https://www.digitalocean.com/community/tutorials/how-to-install-mysql-on-ubuntu-20-04
