/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.platchket.helloworld;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Elliott
 */
public class HelloWorld {
    
    public static void main( String args[]) throws FileNotFoundException, UnsupportedEncodingException {
        
        PrintWriter writer;
        
        writer = new PrintWriter("students.txt", "UTF-8");
        String beginning = "INSERT INTO student (student_id, student_code, first_name, last_name, student_email, date_of_birth, student_image_id, disabled ) VALUES(";
        String out = "";
        
        for( int i = 1; i < 101 ; i ++) {
            
            out = beginning + (i) + ", 'code: " + (i) + "', 'fname" + (i) + "', 'lname" + (i) + "', 'example" + (i) + "@456.com', '2000-03-01 00:00:00', NULL, false);";
            writer.println(out);
        }
    
    
    writer.close();
    
        writer = new PrintWriter("departments.txt", "UTF-8");
       
        beginning = "INSERT INTO department(department_id, department_code, description, disabled) values(";
        out = "";
        for( int i = 1; i < 11 ; i ++) {
            out = beginning + i + ",'department:" + i + "','description:" + i + "',false);";
            writer.println(out);
        }
        
        writer.close();
        
        
        writer = new PrintWriter("teachers.txt", "UTF-8");
       
        beginning = "INSERT INTO teacher (teacher_id, teacher_code, department_id, first_name, last_name, teacher_email, disabled) VALUES(";
        out = "";
        for( int i = 1; i < 11 ; i ++) {
            out = beginning + i + ",'code:" + i + "'," + (i%10 + 1) +",'fname:" + i + "','lname:" + i + "', 'example" + (i) + "@456.com',false);";
            writer.println(out);
        }
        
        writer.close();
        
        writer = new PrintWriter("courses.txt", "UTF-8");
        beginning = "INSERT INTO course(course_id, course_code, teacher_id, description, course_cost, disabled) VALUES (";
        out = "";
        
        for( int i = 1; i < 11 ; i ++) {
            
            out = beginning + i + ",'code:" + i + "'," + (i%10 + 1) +",'description:" + i +"'," + 100.99 + ", false);";
            writer.println(out);
            
        }
        writer.close();
        
        writer = new PrintWriter("terms.txt", "UTF-8");
        beginning = "INSERT INTO term (term_id, term_code, description, disabled) VALUES (";
        out = "";
        
        for( int i = 1; i < 3 ; i ++) {
            
            out = beginning + i + ",'code:" + i + "'," +"'description:" + i +"', false);";
            writer.println(out);
            
        }
        writer.close();
        
        writer = new PrintWriter("registration.txt", "UTF-8");
        beginning = "INSERT INTO registration (registration_id, registration_code, course_id, student_id, term_id) VALUES (";
        out = "";
        
        for( int i = 1; i < 201 ; i ++) {
            
            out = beginning + i + ",'code:" + i + "'," +((i%10)+1) + "," + ((i%100)+1) + ", " + ((i%2)+1) +");";
            writer.println(out);
            
        }
        
        writer.close();
        
        writer = new PrintWriter("studentimages.txt", "UTF-8");
        beginning = "INSERT INTO student_image (student_image_id, student_image_code) VALUES (";
        out = "";
        
        for( int i = 1; i < 101 ; i ++) {
            
            //out = beginning + i + ",'code: " + i + "'" + ", NULL);";
            out = beginning + i + ",'code: " + i + "');";
            writer.println(out);
            
        }
        
        writer.close();

    }
    
    
    /*
    
    Make sure the password in application.properties matches yuor root password.
    
    From the MySQL Client Line, run:
    
    mysql> drop database test1;
    mysql> create database test1;
    
    
    The project will need to be run as a Spring application to generate the schema.
    
    Order to import tables. For each import, do:
    
    mysql -u root -p test1 < terms.sql
    mysql -u root -p test1 < departments.sql
    mysql -u root -p test1 < teachers.sql
    mysql -u root -p test1 < students.sql
    mysql -u root -p test1 < images.sql
    mysql -u root -p test1 < courses.sql
    mysql -u root -p test1 < registrations.sql
   
    You will need to enter your password after each command.
    
    */
    
    
    
}
